package gui.services;

import gui.mainEngine.Gui;

public interface FileService {

	public void createProject(Gui gui);

	public void loadProject(Gui gui);

	public void editProject(Gui gui);
}
