package gui.services;

import java.awt.event.MouseEvent;

import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;

import gui.tableElements.commons.JvTable;
import gui.tableElements.tableRenderers.IDUTableRenderer;

public interface EventHandlerService {

	public void valueChangesInTree(TreeSelectionEvent e);
	
	public void treeRelease(MouseEvent e, JTree tree);	
	
	public void generalTableIDUClick(MouseEvent e, IDUTableRenderer renderer);
	
	public void generalTablePhasesClick(MouseEvent e);
	
	public void zoomTableClick(MouseEvent e);
	
	public void zoomTableForClusterClick(MouseEvent e);
	
	public void generalTableIDURelease(MouseEvent e, JvTable table);
	
	public void generalTablePhasesRelease(MouseEvent e, JvTable table);
	
	public void zoomTableRelease(MouseEvent e, JvTable table);
	
	public void zoomTableForClusterRelease(MouseEvent e, JvTable table);
	
	public void headerTableIDUClick(MouseEvent e, IDUTableRenderer renderer, JvTable table);
	
	public void headerTablePhasesClick(MouseEvent e, JvTable table);
	
	public void zoomHeaderTableClick(MouseEvent e, JvTable table);
	
	public void zoomHeaderTableForClusterClick(MouseEvent e, JvTable table);
	
	public void headerTableIDURelease(MouseEvent e, IDUTableRenderer renderer, JvTable table);
	
	public void headerTablePhasesRelease(MouseEvent e, JvTable table);
	
	public void zoomHeaderTableRelease(MouseEvent e, JvTable table);
	
	public void zoomHeaderTableForClusterRelease(MouseEvent e, JvTable table);
	
	public void clearSelection();
	
	public void showDetailsOfSelection(JTable jTable , JvTable table);
	
	public void clearSelectionOfLifeTimeTable();
	
	public void showDetails();
	
	public void clearColumnSelection(JvTable table, IDUTableRenderer renderer);
	
	public void showDetailsOfPhase(JvTable table);
	
	public void repaintLifeTimeTable();
	
}
