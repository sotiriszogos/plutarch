package gui.services;


public interface ChangeScreenService {

	public void zoomIn();

	public void zoomOut();

	public void enLarge();

	public void unDo();

	public void setSameWidth();

	public void setOverTime();
}
