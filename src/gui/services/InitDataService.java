package gui.services;

public interface InitDataService {

	public void initDataOfDetailedTable();
	
	public void initDataOfPLD();
	
	public void initDataOfPhasesPLD();
	
	public void initDataOfPhasesWithClustersPLD();
}
