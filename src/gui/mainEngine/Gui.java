package gui.mainEngine;


//try to extract relationship beetween gui and pplSchema and pplTransition
import gui.listeners.ButtonListener;
import gui.listeners.MenuItemListener;
import gui.listeners.TreeListener;
import gui.tableElements.commons.JvTable;
import gui.tableElements.commons.MyTableModel;
import java.awt.Color;
import java.awt.EventQueue;
import java.util.ArrayList;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import data.dataKeeper.GlobalDataKeeper;


public class Gui extends JFrame  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	private JPanel lifeTimePanel = new JPanel();
	private JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	private MyTableModel zoomModel = null;
	private JvTable LifeTimeTable = null;
	private JvTable zoomAreaTable = null;

	private JScrollPane tmpScrollPane = new JScrollPane();
	private JScrollPane treeScrollPane = new JScrollPane();
	private JScrollPane tmpScrollPaneZoomArea = new JScrollPane();

	private ArrayList<Integer> selectedRows = new ArrayList<Integer>();

	private JTree tablesTree = new JTree();
	private JPanel sideMenu = new JPanel();
	private JPanel tablesTreePanel = new JPanel();
	private JPanel descriptionPanel = new JPanel();
	private JLabel treeLabel;
	private JLabel generalTableLabel;
	private JLabel zoomAreaLabel;
	private JLabel descriptionLabel;
	private JTextArea descriptionText;
	private JButton zoomInButton;
	private JButton zoomOutButton;
	private JButton uniformlyDistributedButton;
	private JButton notUniformlyDistributedButton;
	private JButton showThisToPopup;

	private boolean showingPld = false;
	private boolean currentLevelized;

	private JButton undoButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					// return;
					e.printStackTrace();
				}

			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Gui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		createMenu();
		createSideMenuView();
		createContentView();
		addButtonsAndLabelsToView();
		pack();
		setBounds(30, 30, 1300, 700);
	}



	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public void setTabbedPane(JTabbedPane tabbedPane) {
		this.tabbedPane = tabbedPane;
	}

	public JButton getZoomInButton() {
		return zoomInButton;
	}

	public void setZoomInButton(JButton zoomInButton) {
		this.zoomInButton = zoomInButton;
	}

	public JButton getZoomOutButton() {
		return zoomOutButton;
	}

	public void setZoomOutButton(JButton zoomOutButton) {
		this.zoomOutButton = zoomOutButton;
	}
	
	public JvTable getZoomAreaTable() {
		return zoomAreaTable;
	}

	public void setZoomAreaTable(JvTable zoomAreaTable) {
		this.zoomAreaTable = zoomAreaTable;
	}


	public JvTable getLifeTimeTable() {
		return LifeTimeTable;
	}

	public void setLifeTimeTable(JvTable lifeTimeTable) {
		LifeTimeTable = lifeTimeTable;
	}

	public boolean isShowingPld() {
		return showingPld;
	}

	public void setShowingPld(boolean showingPld) {
		this.showingPld = showingPld;
	}

	public MyTableModel getZoomModel() {
		return zoomModel;
	}

	public void setZoomModel(MyTableModel zoomModel) {
		this.zoomModel = zoomModel;
	}

	public boolean isCurrentLevelized() {
		return currentLevelized;
	}

	public void setCurrentLevelized(boolean currentLevelized) {
		this.currentLevelized = currentLevelized;
	}
	
	public ArrayList<Integer> getSelectedRows() {
		return selectedRows;
	}

	public void setSelectedRows(ArrayList<Integer> selectedRows) {
		this.selectedRows = selectedRows;
	}

	public JPanel getLifeTimePanel() {
		return lifeTimePanel;
	}

	public void createMenu() {
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenuItem mntmCreateProject = new JMenuItem("Create Project");
		mntmCreateProject.addActionListener(new MenuItemListener(mntmCreateProject, this));
		mnFile.add(mntmCreateProject);

		JMenuItem mntmLoadProject = new JMenuItem("Load Project");
		mntmLoadProject.addActionListener(new MenuItemListener(mntmLoadProject, this));
		mnFile.add(mntmLoadProject);

		JMenuItem mntmEditProject = new JMenuItem("Edit Project");
		mntmEditProject.addActionListener(new MenuItemListener(mntmEditProject, this));
		mnFile.add(mntmEditProject);

		JMenu mnTable = new JMenu("Table");
		menuBar.add(mnTable);

		JMenuItem mntmShowGeneralLifetimeIDU = new JMenuItem("Show PLD");
		mntmShowGeneralLifetimeIDU.addActionListener(new MenuItemListener(mntmShowGeneralLifetimeIDU, this));
		mnTable.add(mntmShowGeneralLifetimeIDU);

		JMenuItem mntmShowGeneralLifetimePhasesPLD = new JMenuItem("Show Phases PLD");
		mntmShowGeneralLifetimePhasesPLD
				.addActionListener(new MenuItemListener(mntmShowGeneralLifetimePhasesPLD, this));
		mnTable.add(mntmShowGeneralLifetimePhasesPLD);

		JMenuItem mntmShowGeneralLifetimePhasesWithClustersPLD = new JMenuItem("Show Phases With Clusters PLD");
		mntmShowGeneralLifetimePhasesWithClustersPLD
				.addActionListener(new MenuItemListener(mntmShowGeneralLifetimePhasesWithClustersPLD, this));
		mnTable.add(mntmShowGeneralLifetimePhasesWithClustersPLD);
		
		JMenuItem mntmShowLifetimeTable = new JMenuItem("Show Full Detailed LifeTime Table");
		mntmShowLifetimeTable.addActionListener(new MenuItemListener(mntmShowLifetimeTable, this));
		mnTable.add(mntmShowLifetimeTable);

		JButton buttonHelp = new JButton("Help");
		buttonHelp.addActionListener(new ButtonListener(buttonHelp, this));

		JMenu mnProject = new JMenu("Project");
		menuBar.add(mnProject);

		JMenuItem mntmInfo = new JMenuItem("Info");
		mntmInfo.addActionListener(new MenuItemListener(mntmInfo, this));
		mnProject.add(mntmInfo);

		buttonHelp.setBounds(900, 900, 80, 40);
		menuBar.add(buttonHelp);
	}

	public void createSideMenuView() {
		sideMenu.setBounds(0, 0, 280, 600);
		sideMenu.setBackground(Color.DARK_GRAY);

		GroupLayout gl_sideMenu = new GroupLayout(sideMenu);
		gl_sideMenu.setHorizontalGroup(gl_sideMenu.createParallelGroup(Alignment.LEADING));
		gl_sideMenu.setVerticalGroup(gl_sideMenu.createParallelGroup(Alignment.LEADING));

		sideMenu.setLayout(gl_sideMenu);

		tablesTreePanel.setBounds(10, 400, 260, 180);
		tablesTreePanel.setBackground(Color.LIGHT_GRAY);

		GroupLayout gl_tablesTreePanel = new GroupLayout(tablesTreePanel);
		gl_tablesTreePanel.setHorizontalGroup(gl_tablesTreePanel.createParallelGroup(Alignment.LEADING));
		gl_tablesTreePanel.setVerticalGroup(gl_tablesTreePanel.createParallelGroup(Alignment.LEADING));

		tablesTreePanel.setLayout(gl_tablesTreePanel);

		treeLabel = new JLabel();
		treeLabel.setBounds(10, 370, 260, 40);
		treeLabel.setForeground(Color.WHITE);
		treeLabel.setText("Tree");

		descriptionPanel.setBounds(10, 190, 260, 180);
		descriptionPanel.setBackground(Color.LIGHT_GRAY);

		GroupLayout gl_descriptionPanel = new GroupLayout(descriptionPanel);
		gl_descriptionPanel.setHorizontalGroup(gl_descriptionPanel.createParallelGroup(Alignment.LEADING));
		gl_descriptionPanel.setVerticalGroup(gl_descriptionPanel.createParallelGroup(Alignment.LEADING));

		descriptionPanel.setLayout(gl_descriptionPanel);
		descriptionText = new JTextArea();
		descriptionText.setBounds(5, 5, 250, 170);
		descriptionText.setForeground(Color.BLACK);
		descriptionText.setText("");
		descriptionText.setBackground(Color.LIGHT_GRAY);

		descriptionPanel.add(descriptionText);
		descriptionLabel = new JLabel();
		descriptionLabel.setBounds(10, 160, 260, 40);
		descriptionLabel.setForeground(Color.WHITE);
		descriptionLabel.setText("Description");

		sideMenu.add(treeLabel);
		sideMenu.add(tablesTreePanel);
		sideMenu.add(descriptionLabel);
		sideMenu.add(descriptionPanel);

		lifeTimePanel.add(sideMenu);
	}

	public void createContentView() {
		contentPane = new JPanel();

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addComponent(tabbedPane,
				Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 1474, Short.MAX_VALUE));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addComponent(tabbedPane,
				Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 771, Short.MAX_VALUE));

		tabbedPane.addTab("LifeTime Table", null, lifeTimePanel, null);

		GroupLayout gl_lifeTimePanel = new GroupLayout(lifeTimePanel);
		gl_lifeTimePanel.setHorizontalGroup(
				gl_lifeTimePanel.createParallelGroup(Alignment.LEADING).addGap(0, 1469, Short.MAX_VALUE));
		gl_lifeTimePanel.setVerticalGroup(
				gl_lifeTimePanel.createParallelGroup(Alignment.LEADING).addGap(0, 743, Short.MAX_VALUE));
		lifeTimePanel.setLayout(gl_lifeTimePanel);

		contentPane.setLayout(gl_contentPane);
	}

	public void addButtonsAndLabelsToView() {
		generalTableLabel = new JLabel("Parallel Lives Diagram");
		generalTableLabel.setBounds(300, 0, 150, 30);
		generalTableLabel.setForeground(Color.BLACK);

		zoomAreaLabel = new JLabel();
		zoomAreaLabel.setText("<HTML>Z<br>o<br>o<br>m<br><br>A<br>r<br>e<br>a</HTML>");
		zoomAreaLabel.setBounds(1255, 325, 15, 300);
		zoomAreaLabel.setForeground(Color.BLACK);

		zoomInButton = new JButton("Zoom In");
		zoomInButton.setBounds(1000, 560, 100, 30);
		zoomInButton.addActionListener(new ButtonListener(zoomInButton, this));
		zoomInButton.setVisible(false);

		zoomOutButton = new JButton("Zoom Out");
		zoomOutButton.setBounds(1110, 560, 100, 30);
		zoomOutButton.addActionListener(new ButtonListener(zoomOutButton, this));
		zoomOutButton.setVisible(false);

		showThisToPopup = new JButton("Enlarge");
		showThisToPopup.setBounds(800, 560, 100, 30);
		showThisToPopup.addActionListener(new ButtonListener(showThisToPopup, this));
		showThisToPopup.setVisible(false);

		undoButton = new JButton("Undo");
		undoButton.setBounds(680, 560, 100, 30);
		undoButton.addActionListener(new ButtonListener(undoButton, this));
		undoButton.setVisible(false);

		uniformlyDistributedButton = new JButton("Same Width");
		uniformlyDistributedButton.setBounds(980, 0, 120, 30);
		uniformlyDistributedButton.addActionListener(new ButtonListener(uniformlyDistributedButton, this));
		uniformlyDistributedButton.setVisible(false);

		notUniformlyDistributedButton = new JButton("Over Time");
		notUniformlyDistributedButton.setBounds(1100, 0, 120, 30);
		notUniformlyDistributedButton.addActionListener(new ButtonListener(notUniformlyDistributedButton, this));
		notUniformlyDistributedButton.setVisible(false);

		lifeTimePanel.add(zoomInButton);
		lifeTimePanel.add(undoButton);
		lifeTimePanel.add(zoomOutButton);
		lifeTimePanel.add(uniformlyDistributedButton);
		lifeTimePanel.add(notUniformlyDistributedButton);
		lifeTimePanel.add(showThisToPopup);
		lifeTimePanel.add(zoomAreaLabel);
		lifeTimePanel.add(generalTableLabel);
	}
	
	public void printDescription(String description) {
		descriptionText.setText(description);
	}

	public void printGeneralTableIDU(JvTable generalTable) {
		tabbedPane.setSelectedIndex(0);
		showingPld = true;
		zoomInButton.setVisible(true);
		zoomOutButton.setVisible(true);
		showThisToPopup.setVisible(true);
		
		zoomAreaTable = generalTable;
		tmpScrollPaneZoomArea.setViewportView(zoomAreaTable);
		tmpScrollPaneZoomArea.setAlignmentX(0);
		tmpScrollPaneZoomArea.setAlignmentY(0);
		tmpScrollPaneZoomArea.setBounds(300, 300, 950, 250);
		tmpScrollPaneZoomArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		tmpScrollPaneZoomArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		lifeTimePanel.setCursor(getCursor());
		lifeTimePanel.add(tmpScrollPaneZoomArea);
	}

	public void printPhasesTable(JvTable phasesTable) {
		
		uniformlyDistributedButton.setVisible(true);
		notUniformlyDistributedButton.setVisible(true);
		
		tabbedPane.setSelectedIndex(0);
		LifeTimeTable = phasesTable;
		tmpScrollPane.setViewportView(LifeTimeTable);
		tmpScrollPane.setAlignmentX(0);
		tmpScrollPane.setAlignmentY(0);
		tmpScrollPane.setBounds(300, 30, 950, 265);
		tmpScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		tmpScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		lifeTimePanel.setCursor(getCursor());
		lifeTimePanel.add(tmpScrollPane);
	}
	
	public void printZoomTable(JvTable zoomTable) {
		tabbedPane.setSelectedIndex(0);
		showingPld = false;
		zoomAreaTable = zoomTable;

		tmpScrollPaneZoomArea.setViewportView(zoomAreaTable);
		tmpScrollPaneZoomArea.setAlignmentX(0);
		tmpScrollPaneZoomArea.setAlignmentY(0);
		tmpScrollPaneZoomArea.setBounds(300, 300, 950, 250);
		tmpScrollPaneZoomArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		tmpScrollPaneZoomArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		lifeTimePanel.setCursor(getCursor());
		lifeTimePanel.add(tmpScrollPaneZoomArea);
		
	}
	
	public void printZoomTableForCluster(JvTable zoomTableForCluster) {
		tabbedPane.setSelectedIndex(0);
		showingPld = false;
		undoButton.setVisible(true);
		zoomAreaTable = zoomTableForCluster;
		
		tmpScrollPaneZoomArea.setViewportView(zoomAreaTable);
		tmpScrollPaneZoomArea.setAlignmentX(0);
		tmpScrollPaneZoomArea.setAlignmentY(0);
		tmpScrollPaneZoomArea.setBounds(300, 300, 950, 250);
		tmpScrollPaneZoomArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		tmpScrollPaneZoomArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		lifeTimePanel.setCursor(getCursor());
		lifeTimePanel.add(tmpScrollPaneZoomArea);
	}
	
	public void printDetailedTable(JvTable tmpLifeTimeTable) {
		
		tabbedPane.setSelectedIndex(0);
		JScrollPane detailedScrollPane = new JScrollPane();
		detailedScrollPane.setViewportView(tmpLifeTimeTable);
		detailedScrollPane.setAlignmentX(0);
		detailedScrollPane.setAlignmentY(0);
		detailedScrollPane.setBounds(0, 0, 1280, 650);
		detailedScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		detailedScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		detailedScrollPane.setCursor(getCursor());

		JDialog detailedDialog = new JDialog();
		detailedDialog.setBounds(100, 100, 1300, 700);

		JPanel panelToAdd = new JPanel();

		GroupLayout gl_panel = new GroupLayout(panelToAdd);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING));
		panelToAdd.setLayout(gl_panel);

		panelToAdd.add(detailedScrollPane);
		detailedDialog.getContentPane().add(panelToAdd);
		detailedDialog.setVisible(true);
	}
	
	public void fillTableTree(JTree tree, String descriptionText) {
		tablesTree = tree;
		tablesTree.addTreeSelectionListener(new TreeListener(tablesTree, this));
		tablesTree.addMouseListener(new TreeListener(tablesTree, this));

		treeScrollPane.setViewportView(tablesTree);
		treeScrollPane.setBounds(5, 5, 250, 170);
		treeScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		treeScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		tablesTreePanel.add(treeScrollPane);

		treeLabel.setText(descriptionText);

		sideMenu.revalidate();
		sideMenu.repaint();
	}
	
	public void zoom(int rowHeight, int columnWidth) {
		zoomAreaTable.setZoom(rowHeight, columnWidth);
	}
	
	public void setWidth(int columnWidth) {
		LifeTimeTable.uniformlyDistributed(columnWidth);
	}
	
	public void enLargeWidth(GlobalDataKeeper globalDataKeeper) {
		LifeTimeTable.notUniformlyDistributed(globalDataKeeper);
	}
	
	public void zoomAreaTableRepaint() {
		zoomAreaTable.repaint();
	}
	
	public void lifeTimeTableRepaint() {
		LifeTimeTable.repaint();
	}
	
	public void setDescription(String description) {
		descriptionText.setText(description);
	}

}
