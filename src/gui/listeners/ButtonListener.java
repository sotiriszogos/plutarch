package gui.listeners;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import data.businessLogic.ChangeScreenServiceImpl;
import data.businessLogic.ShowMessageServiceImpl;
import gui.mainEngine.Gui;
import gui.services.ChangeScreenService;
import gui.services.ShowMessageService;

public class ButtonListener extends ItemListener<JButton> implements ActionListener {
	private ChangeScreenService changeScreenService;
	private ShowMessageService showMessageService;
	private Gui gui;
	
	public ButtonListener(JButton button, Gui gui) {
		super(button);
		this.changeScreenService = new ChangeScreenServiceImpl(gui);
		this.showMessageService = new ShowMessageServiceImpl();
		this.gui = gui;
	}

	public void actionPerformed(ActionEvent e) {
		String buttonCommand = getItem().getActionCommand();

		switch (buttonCommand) {
		case "Help": {
			this.showMessageService.help();
			break;
		}

		case "Zoom In": {
			this.changeScreenService.zoomIn();
			break;
		}

		case "Zoom Out": {
			this.changeScreenService.zoomOut();
			break;
		}

		case "Enlarge": {
			this.changeScreenService.enLarge();
			break;
		}

		case "Undo": {
			this.changeScreenService.unDo();
			break;
		}
		
		case "Same Width": {
			this.changeScreenService.setSameWidth();
			break;
		}
		
		case "Over Time": {
			this.changeScreenService.setOverTime();
			break;
		}

		default: {
			throw new IllegalStateException(String.format("There isnt a Button '%s'", buttonCommand));
		}
		}
	}
}
