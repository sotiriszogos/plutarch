package gui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JTable;

import data.businessLogic.EventHandlerServiceImpl;
import data.businessLogic.FileServiceImpl;
import data.businessLogic.InitDataServiceImpl;
import data.businessLogic.ShowMessageServiceImpl;
import gui.mainEngine.Gui;
import gui.services.EventHandlerService;
import gui.services.FileService;
import gui.services.InitDataService;
import gui.services.ShowMessageService;
import gui.tableElements.commons.JvTable;
import gui.tableElements.tableRenderers.IDUTableRenderer;

public class MenuItemListener extends ItemListener<JMenuItem> implements ActionListener {
	private FileService fileService;
	private ShowMessageService showMessageService;
	private InitDataService initDataService;
	private EventHandlerService eventHandlerService;
	private JvTable table;
	private JTable jTable;
	private Gui gui;
	private IDUTableRenderer renderer;

	public MenuItemListener(JMenuItem item, Gui gui) {
		super(item);
		this.gui = gui;
		this.eventHandlerService = new EventHandlerServiceImpl(gui);
		this.fileService = new FileServiceImpl();
		this.showMessageService = new ShowMessageServiceImpl();
		this.initDataService = new InitDataServiceImpl(gui);
	}
	
	public MenuItemListener(JMenuItem item, Gui gui, JvTable table, JTable jTable, IDUTableRenderer renderer) {
		super(item);
		this.eventHandlerService = new EventHandlerServiceImpl(gui);
		this.table = table;
		this.jTable = jTable;
		this.renderer = renderer;
	}
	
	public MenuItemListener(JMenuItem item, Gui gui, JvTable table, JTable jTable) {
		super(item);
		this.eventHandlerService = new EventHandlerServiceImpl(gui);
		this.table = table;
		this.jTable = jTable;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String menuItemCommand = getItem().getActionCommand();
		switch (menuItemCommand) {
		case "Create Project": {
			this.fileService.createProject(gui);
			break;
		}
		case "Load Project": {
			this.fileService.loadProject(gui);
			break;
		}

		case "Edit Project": {
			this.fileService.editProject(gui);
			break;
		}
		case "Show Full Detailed LifeTime Table": {
			this.initDataService.initDataOfDetailedTable();
			break;
		}
		case "Show PLD": {
			this.initDataService.initDataOfPLD();
			break;
		}

		case "Show Phases PLD": {
			this.initDataService.initDataOfPhasesPLD();
			break;
		}

		case "Show Phases With Clusters PLD": {
			this.initDataService.initDataOfPhasesWithClustersPLD();
			break;
		}

		case "Info": {
			this.showMessageService.printInfo();
			break;
		}
		
		case "Clear Selection": {
			this.eventHandlerService.clearSelection();
			break;
		}
		
		case "Show Details for the selection": {
			this.eventHandlerService.showDetailsOfSelection(jTable, table);
			break;
		}
		
		case "Clear Selection of LifeTimeTable": {
			this.eventHandlerService.clearSelectionOfLifeTimeTable();
			break;
		}
		
		case "Show Details": {
			this.eventHandlerService.showDetails();
			break;
		}
		
		case "Clear Column Selection": {
			this.eventHandlerService.clearColumnSelection(table,renderer);
			break;
		}
		
		case "Show Details for this Phase" : {
			this.eventHandlerService.showDetailsOfPhase(table);
			break;
		}
		
		case "Show This into the Table" : {
			this.eventHandlerService.repaintLifeTimeTable();
			break;
		}

		default: {
			throw new IllegalStateException(String.format("There isnt MenuItem '%s'", menuItemCommand));
		}
		}
	}

}
