package gui.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.table.JTableHeader;

import data.businessLogic.EventHandlerServiceImpl;
import gui.mainEngine.Gui;
import gui.services.EventHandlerService;
import gui.tableElements.commons.JvTable;
import gui.tableElements.tableRenderers.IDUTableRenderer;

public class TableHeaderListener extends ItemListener<JTableHeader> implements MouseListener {
	private EventHandlerService eventHandlerService;
	private JvTable table;
	private IDUTableRenderer renderer;
	
	public TableHeaderListener(JTableHeader header, Gui gui, JvTable table) {
		super(header);
		this.eventHandlerService = new EventHandlerServiceImpl(gui);
		this.table = table;
	}
	
	public TableHeaderListener(JTableHeader header, Gui gui, JvTable table,IDUTableRenderer renderer) {
		super(header);
		this.eventHandlerService = new EventHandlerServiceImpl(gui);
		this.renderer = renderer;
		this.table = table;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		String name = getItem().getName();
		switch(name) {
		case "headerTableIDU" :
		{
			eventHandlerService.headerTableIDUClick(e, renderer, table);
			break;
		}
		case "headerTablePhases" :
		{
			eventHandlerService.headerTablePhasesClick(e, table);
			break;
		}
		case "zoomHeaderTable" :
		{
			eventHandlerService.zoomHeaderTableClick(e, table);
			break;
		}
		case "zoomHeaderTableForCluster" :
		{
			eventHandlerService.zoomHeaderTableForClusterClick(e, table);
			break;
		}
		default: {
			throw new IllegalStateException(String.format("There isnt HeaderTable '%s'", name));
		}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		String name = getItem().getName();
		switch(name) {
		case "headerTableIDU" :
		{
			eventHandlerService.headerTableIDURelease(e,renderer,table);
			break;
		}
		case "headerTablePhases" :
		{
			eventHandlerService.headerTablePhasesRelease(e,table);
			break;
		}
		case "zoomHeaderTable" :
		{
			eventHandlerService.zoomHeaderTableRelease(e, table);
			break;
		}
		case "zoomHeaderTableForCluster" :
		{
			eventHandlerService.zoomHeaderTableForClusterRelease(e, table);
			break;
		}
		default: {
			throw new IllegalStateException(String.format("There isnt HeaderTable '%s'", name));
		}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}
