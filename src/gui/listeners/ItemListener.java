package gui.listeners;

public class ItemListener<T> {
	private T item;

	public ItemListener(T item) {
		this.item = item;
	}

	public T getItem() {
		return item;
	}
}
