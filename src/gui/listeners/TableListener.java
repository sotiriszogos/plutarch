package gui.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTable;

import data.businessLogic.EventHandlerServiceImpl;
import gui.mainEngine.Gui;
import gui.services.EventHandlerService;
import gui.tableElements.commons.JvTable;
import gui.tableElements.tableRenderers.IDUTableRenderer;

public class TableListener extends ItemListener<JvTable> implements MouseListener {
	private IDUTableRenderer renderer;
	private JvTable table;
	private EventHandlerService eventHandlerService;
	
	public TableListener(JvTable table, Gui gui) {
		super(table);
		this.table = table;
		this.eventHandlerService = new EventHandlerServiceImpl(gui);
	}
	
	
	public TableListener(JvTable table, Gui gui, IDUTableRenderer renderer) {
		super(table);
		this.eventHandlerService = new EventHandlerServiceImpl(gui);
		this.renderer = renderer;
		this.table = table;
	}
	

	@Override
	public void mouseClicked(MouseEvent e) {
		String name = getItem().getName();
		switch(name) {
		case "generalTableIDU" :
		{
			eventHandlerService.generalTableIDUClick(e, renderer);
			break;
		}
		case "generalTablePhases" :
		{
			eventHandlerService.generalTablePhasesClick(e);
			break;
		}
		case "zoomTable" :
		{
			eventHandlerService.zoomTableClick(e);
			break;
		}
		case "zoomTableForCluster" :
		{
			eventHandlerService.zoomTableForClusterClick(e);
			break;
		}
		
		default: {
			throw new IllegalStateException(String.format("There isnt Table '%s'", name));
		}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		String name = getItem().getName();
		switch(name) {
		case "generalTableIDU" :
		{
			eventHandlerService.generalTableIDURelease(e,table);
			break;
		}
		case "generalTablePhases" :
		{
			eventHandlerService.generalTablePhasesRelease(e,table);
			break;
		}
		case "zoomTable" :
		{
			eventHandlerService.zoomTableRelease(e,table);
			break;
		}
		case "zoomTableForCluster" :
		{
			eventHandlerService.zoomTableForClusterRelease(e,table);
			break;
		}
		default: {
			throw new IllegalStateException(String.format("There isnt Table '%s'", name));
		}
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
