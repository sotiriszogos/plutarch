package gui.listeners;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import gui.mainEngine.Gui;

public class RowListener implements ListSelectionListener {
	private Gui gui;
	public RowListener(Gui gui) {
		this.gui= gui;
	}
	
	public void valueChanged(ListSelectionEvent event) {
		if (event.getValueIsAdjusting()) {
			return;
		}

		int selectedRow = gui.getLifeTimeTable().getSelectedRow();
		gui.getSelectedRows().add(selectedRow);

	}
}