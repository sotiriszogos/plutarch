package gui.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import data.businessLogic.EventHandlerServiceImpl;
import gui.mainEngine.Gui;
import gui.services.EventHandlerService;

public class TreeListener extends ItemListener<JTree> implements TreeSelectionListener, MouseListener {

	private EventHandlerService eventHandlerService;
	private JTree tree;

	public TreeListener(JTree tree, Gui gui) {
		super(tree);
		this.eventHandlerService = new EventHandlerServiceImpl(gui);
		this.tree = tree;
	}

	@Override
	public void valueChanged(TreeSelectionEvent e) {

		this.eventHandlerService.valueChangesInTree(e);

	}


	@Override
	public void mouseReleased(MouseEvent e) {

		this.eventHandlerService.treeRelease(e,tree);

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	

}
