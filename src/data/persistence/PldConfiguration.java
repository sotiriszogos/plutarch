package data.persistence;

public class PldConfiguration {

	private Float timeWeight = null;
	private Float changeWeight = null;
	private Double birthWeight = null;
	private Double deathWeight = null;
	private Double changeWeightCl = null;

	private Integer numberOfPhases = null;
	private Integer numberOfClusters = null;
	private Boolean preProcessingTime = null;
	private Boolean preProcessingChange = null;
	
	public PldConfiguration(float timeWeight,float changeWeight , int numberOfPhases, 
			 boolean preProcessingTime , boolean preProcessingChange) {
		this.timeWeight = timeWeight;
		this.changeWeight = changeWeight;
		this.numberOfPhases = numberOfPhases;
		this.preProcessingTime = preProcessingTime;
		this.preProcessingChange = preProcessingChange;
		
	}
	
	public PldConfiguration(float timeWeight,float changeWeight , double birthWeight ,double deathWeight,double changeWeightCl,int numberOfPhases, 
			int numberOfClusters, boolean preProcessingTime , boolean preProcessingChange) {
		
		this.timeWeight = timeWeight;
		this.changeWeight = changeWeight;
		this.birthWeight =  birthWeight;
		this.deathWeight = deathWeight;
		this.changeWeightCl = changeWeightCl;
		this.numberOfPhases = numberOfPhases;
		this.numberOfClusters = numberOfClusters;
		this.preProcessingTime = preProcessingTime;
		this.preProcessingChange = preProcessingChange;
	}
	
	public PldConfiguration() {
		
	}
	
	public Float getTimeWeight() {
		return timeWeight;
	}
	public void setTimeWeight(Float timeWeight) {
		this.timeWeight = timeWeight;
	}
	public Float getChangeWeight() {
		return changeWeight;
	}
	public void setChangeWeight(Float changeWeight) {
		this.changeWeight = changeWeight;
	}
	public Double getBirthWeight() {
		return birthWeight;
	}
	public void setBirthWeight(Double birthWeight) {
		this.birthWeight = birthWeight;
	}
	public Double getDeathWeight() {
		return deathWeight;
	}
	public void setDeathWeight(Double deathWeight) {
		this.deathWeight = deathWeight;
	}
	public Double getChangeWeightCl() {
		return changeWeightCl;
	}
	public void setChangeWeightCl(Double changeWeightCl) {
		this.changeWeightCl = changeWeightCl;
	}
	public Integer getNumberOfPhases() {
		return numberOfPhases;
	}
	public void setNumberOfPhases(Integer numberOfPhases) {
		this.numberOfPhases = numberOfPhases;
	}
	public Integer getNumberOfClusters() {
		return numberOfClusters;
	}
	public void setNumberOfClusters(Integer numberOfClusters) {
		this.numberOfClusters = numberOfClusters;
	}
	public Boolean getPreProcessingTime() {
		return preProcessingTime;
	}
	public void setPreProcessingTime(Boolean preProcessingTime) {
		this.preProcessingTime = preProcessingTime;
	}
	public Boolean getPreProcessingChange() {
		return preProcessingChange;
	}
	public void setPreProcessingChange(Boolean preProcessingChange) {
		this.preProcessingChange = preProcessingChange;
	}
	
	
}
