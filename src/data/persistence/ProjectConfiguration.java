package data.persistence;

public class ProjectConfiguration {

	private String projectName = "";
	private String datasetTxt = "";
	private String inputCsv = "";
	private String outputAssessment1 = "";
	private String outputAssessment2 = "";
	private String transitionsFile = "";
	private String project = "";
	private String currentProject = null;
	
	public ProjectConfiguration() {
		
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getDatasetTxt() {
		return datasetTxt;
	}

	public void setDatasetTxt(String datasetTxt) {
		this.datasetTxt = datasetTxt;
	}

	public String getInputCsv() {
		return inputCsv;
	}

	public void setInputCsv(String inputCsv) {
		this.inputCsv = inputCsv;
	}

	public String getOutputAssessment1() {
		return outputAssessment1;
	}

	public void setOutputAssessment1(String outputAssessment1) {
		this.outputAssessment1 = outputAssessment1;
	}

	public String getOutputAssessment2() {
		return outputAssessment2;
	}

	public void setOutputAssessment2(String outputAssessment2) {
		this.outputAssessment2 = outputAssessment2;
	}

	public String getTransitionsFile() {
		return transitionsFile;
	}

	public void setTransitionsFile(String transitionsFile) {
		this.transitionsFile = transitionsFile;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getCurrentProject() {
		return currentProject;
	}

	public void setCurrentProject(String currentProject) {
		this.currentProject = currentProject;
	}
	
	
}
