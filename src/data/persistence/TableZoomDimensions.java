package data.persistence;

public class TableZoomDimensions {
	private String[] finalColumnsZoomArea = null;
	private String[][] finalRowsZoomArea = null;
	private Integer[] segmentSizeZoomArea = new Integer[4];
	
	
	public TableZoomDimensions() {
		
	}
	
	public TableZoomDimensions(String[] finalColumnsZoomArea, String[][] finalRowsZoomArea, Integer[] segmentSizeZoomArea) {
		this.finalColumnsZoomArea = finalColumnsZoomArea;
		this.finalRowsZoomArea = finalRowsZoomArea;
		this.segmentSizeZoomArea = segmentSizeZoomArea;
	}
	
	public String[] getFinalColumnsZoomArea() {
		return finalColumnsZoomArea;
	}
	public void setFinalColumnsZoomArea(String[] finalColumnsZoomArea) {
		this.finalColumnsZoomArea = finalColumnsZoomArea;
	}
	public String[][] getFinalRowsZoomArea() {
		return finalRowsZoomArea;
	}
	public void setFinalRowsZoomArea(String[][] finalRowsZoomArea) {
		this.finalRowsZoomArea = finalRowsZoomArea;
	}
	public Integer[] getSegmentSizeZoomArea() {
		return segmentSizeZoomArea;
	}
	public void setSegmentSizeZoomArea(Integer[] segmentSizeZoomArea) {
		this.segmentSizeZoomArea = segmentSizeZoomArea;
	}
	
	
}
