package data.persistence;

import java.util.ArrayList;

public class TableDataArea {

	private int rowHeight = 1;
	private int columnWidth = 1;
	private int selectedColumn = -1;
	private int selectedColumnZoomArea = -1;
	private int wholeCol = -1;
	private int wholeColZoomArea = -1;
	private boolean currentLevelized;
	private int[] selectedRowsFromMouse;
	private ArrayList<String> selectedFromTree = new ArrayList<String>();
	private ArrayList<String> tablesSelected = new ArrayList<String>();
	private String[] firstLevelUndoColumnsZoomArea = null;
	private String[][] firstLevelUndoRowsZoomArea = null;
	
	public TableDataArea() {
		
	}

	public int getRowHeight() {
		return rowHeight;
	}

	public void setRowHeight(int rowHeight) {
		this.rowHeight = rowHeight;
	}

	public int getColumnWidth() {
		return columnWidth;
	}

	public void setColumnWidth(int columnWidth) {
		this.columnWidth = columnWidth;
	}

	public int getSelectedColumn() {
		return selectedColumn;
	}

	public void setSelectedColumn(int selectedColumn) {
		this.selectedColumn = selectedColumn;
	}

	public int getSelectedColumnZoomArea() {
		return selectedColumnZoomArea;
	}

	public void setSelectedColumnZoomArea(int selectedColumnZoomArea) {
		this.selectedColumnZoomArea = selectedColumnZoomArea;
	}

	public int getWholeCol() {
		return wholeCol;
	}

	public void setWholeCol(int wholeCol) {
		this.wholeCol = wholeCol;
	}

	public int getWholeColZoomArea() {
		return wholeColZoomArea;
	}

	public void setWholeColZoomArea(int wholeColZoomArea) {
		this.wholeColZoomArea = wholeColZoomArea;
	}

	public boolean isCurrentLevelized() {
		return currentLevelized;
	}

	public void setCurrentLevelized(boolean currentLevelized) {
		this.currentLevelized = currentLevelized;
	}

	public ArrayList<String> getSelectedFromTree() {
		return selectedFromTree;
	}

	public void setSelectedFromTree(ArrayList<String> selectedFromTree) {
		this.selectedFromTree = selectedFromTree;
	}

	public String[] getFirstLevelUndoColumnsZoomArea() {
		return firstLevelUndoColumnsZoomArea;
	}

	public void setFirstLevelUndoColumnsZoomArea(String[] firstLevelUndoColumnsZoomArea) {
		this.firstLevelUndoColumnsZoomArea = firstLevelUndoColumnsZoomArea;
	}

	public String[][] getFirstLevelUndoRowsZoomArea() {
		return firstLevelUndoRowsZoomArea;
	}

	public void setFirstLevelUndoRowsZoomArea(String[][] firstLevelUndoRowsZoomArea) {
		this.firstLevelUndoRowsZoomArea = firstLevelUndoRowsZoomArea;
	}

	public int[] getSelectedRowsFromMouse() {
		return selectedRowsFromMouse;
	}

	public void setSelectedRowsFromMouse(int[] selectedRowsFromMouse) {
		this.selectedRowsFromMouse = selectedRowsFromMouse;
	}

	public ArrayList<String> getTablesSelected() {
		return tablesSelected;
	}

	public void setTablesSelected(ArrayList<String> tablesSelected) {
		this.tablesSelected = tablesSelected;
	}

	
	
}
