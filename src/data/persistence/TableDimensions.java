package data.persistence;

public class TableDimensions {
	
	private String[] finalColumns = null;
	private String[][] finalRows = null;
	private Integer[] segmentSize = new Integer[4];
	
	public TableDimensions() {
		
	}
	
	public TableDimensions(String[] finalColumns,String[][] finalRows, Integer[] segmentSize) {
		this.finalColumns = finalColumns;
		this.finalRows = finalRows;
		this.segmentSize = segmentSize;
	}

	public String[] getFinalColumns() {
		return finalColumns;
	}

	public void setFinalColumns(String[] finalColumns) {
		this.finalColumns = finalColumns;
	}

	public String[][] getFinalRows() {
		return finalRows;
	}

	public void setFinalRows(String[][] finalRows) {
		this.finalRows = finalRows;
	}

	public Integer[] getSegmentSize() {
		return segmentSize;
	}

	public void setSegmentSize(Integer[] segmentSize) {
		this.segmentSize = segmentSize;
	}
	
	
}

