package data.persistence;

public class DetailedTableDimensions {
	private String[] detailedColumns = null;
	private String[][] detailedRows = null;
	private Integer[] segmentSizeDetailedTable = new Integer[3];
	
	public DetailedTableDimensions(String[] detailedColumns, String[][] detailedRows, Integer[] segmentSizeDetailedTable )
	{
		this.detailedColumns = detailedColumns;
		this.detailedRows = detailedRows;
		this.segmentSizeDetailedTable = segmentSizeDetailedTable;
	}

	public String[] getDetailedColumns() {
		return detailedColumns;
	}

	public void setDetailedColumns(String[] detailedColumns) {
		this.detailedColumns = detailedColumns;
	}

	public String[][] getDetailedRows() {
		return detailedRows;
	}

	public void setDetailedRows(String[][] detailedRows) {
		this.detailedRows = detailedRows;
	}

	public Integer[] getSegmentSizeDetailedTable() {
		return segmentSizeDetailedTable;
	}

	public void setSegmentSizeDetailedTable(Integer[] segmentSizeDetailedTable) {
		this.segmentSizeDetailedTable = segmentSizeDetailedTable;
	}
	
	
}
