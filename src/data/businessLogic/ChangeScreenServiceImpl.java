package data.businessLogic;

import data.dataKeeper.GlobalDataKeeper;
import data.datawidgetConstruction.TableDataConstructionService;
import data.persistence.TableDataArea;
import data.persistence.TableZoomDimensions;
import gui.dialogs.EnlargeTable;
import gui.mainEngine.Gui;
import gui.services.ChangeScreenService;
import gui.tableElements.tableConstructors.TableConstructionIDU;

public class ChangeScreenServiceImpl implements ChangeScreenService{

	private Gui gui;
	public ChangeScreenServiceImpl(Gui gui) {
		this.gui = gui;
	}
	
	@Override
	public void zoomIn() {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		
		tableDataArea.setRowHeight(tableDataArea.getRowHeight()+2);
		tableDataArea.setColumnWidth(tableDataArea.getColumnWidth()+1);

		gui.zoom(tableDataArea.getRowHeight(), tableDataArea.getColumnWidth());
		
	}

	@Override
	public void zoomOut() {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		
		tableDataArea.setRowHeight(tableDataArea.getRowHeight() - 2);
		tableDataArea.setColumnWidth(tableDataArea.getColumnWidth() - 1);

		if (tableDataArea.getRowHeight() < 1) {
			tableDataArea.setRowHeight(1);

		}
		if (tableDataArea.getColumnWidth() < 1) {
			tableDataArea.setColumnWidth(1);
		}
		gui.zoom(tableDataArea.getRowHeight(), tableDataArea.getColumnWidth());
		
	}

	@Override
	public void enLarge() {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		GlobalDataKeeper globalDataKeeper = fileReader.getGlobalDataKeeper();
		TableConstructionIDU table = new TableConstructionIDU(globalDataKeeper);
		TableZoomDimensions zoomTableDimensions = new TableZoomDimensions(table.constructColumns(),
				table.constructRows(), table.getSegmentSize());
		
		EnlargeTable showEnlargmentPopup = new EnlargeTable(zoomTableDimensions.getFinalRowsZoomArea(), zoomTableDimensions.getFinalColumnsZoomArea(),
				zoomTableDimensions.getSegmentSizeZoomArea());
		showEnlargmentPopup.setBounds(100, 100, 1300, 700);
		showEnlargmentPopup.setVisible(true);
		
	}

	@Override
	public void unDo() {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		GlobalDataKeeper globalDataKeeper = fileReader.getGlobalDataKeeper();
		TableConstructionIDU table = new TableConstructionIDU(globalDataKeeper);
		TableZoomDimensions zoomTableDimensions = new TableZoomDimensions(table.constructColumns(),
				table.constructRows(), table.getSegmentSize());
		
		if (tableDataArea.getFirstLevelUndoColumnsZoomArea() != null) {
			zoomTableDimensions.setFinalColumnsZoomArea(tableDataArea.getFirstLevelUndoColumnsZoomArea());
			zoomTableDimensions.setFinalRowsZoomArea(tableDataArea.getFirstLevelUndoRowsZoomArea());
			TableDataConstructionService tableService = new TableDataConstructionService(globalDataKeeper, gui,tableDataArea);
			
			gui.printZoomTableForCluster(tableService.createZoomAreaTable(zoomTableDimensions));
			
			
		}
		
	}

	@Override
	public void setSameWidth() {
		int columnWidth = 1;
		gui.setWidth(columnWidth);
	}

	@Override
	public void setOverTime() {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		GlobalDataKeeper globalDataKeeper = fileReader.getGlobalDataKeeper();
		gui.enLargeWidth(globalDataKeeper);
		
	}

	
}
