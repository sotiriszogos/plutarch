package data.businessLogic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.antlr.v4.runtime.RecognitionException;

import data.dataKeeper.GlobalDataKeeper;
import data.persistence.ProjectConfiguration;
import data.persistence.TableDataArea;
import gui.mainEngine.Gui;

public class DataFileReaderService {

	private static DataFileReaderService single_instance = null; 
	private GlobalDataKeeper globalDataKeeper = null;
	private ProjectConfiguration projectConfiguration = null;
	private TableDataArea tableDataArea =null;
	
	
	public DataFileReaderService() {
		projectConfiguration = new ProjectConfiguration();
		tableDataArea = new TableDataArea();

	}
	
	public static DataFileReaderService getInstance() 
    { 
        if (single_instance == null) 
            single_instance = new DataFileReaderService(); 
  
        return single_instance; 
    } 
	
	public void importData(String fileName, Gui gui) throws IOException, RecognitionException {

		readFiles(fileName);

		System.out.println("Project Name:" + projectConfiguration.getProjectName());
		System.out.println("Dataset txt:" + projectConfiguration.getDatasetTxt());
		System.out.println("Input Csv:" + projectConfiguration.getInputCsv());
		System.out.println("Output Assessment1:" + projectConfiguration.getOutputAssessment1());
		System.out.println("Output Assessment2:" + projectConfiguration.getOutputAssessment2());
		System.out.println("Transitions File:" + projectConfiguration.getTransitionsFile());

		globalDataKeeper = new GlobalDataKeeper(projectConfiguration.getDatasetTxt(), projectConfiguration.getTransitionsFile());
		globalDataKeeper.setData();
		System.out.println(globalDataKeeper.getAllPPLTables().size());

		
		System.out.println(fileName);
		InitDataServiceImpl tableService = new InitDataServiceImpl(gui);
		tableService.initializationOfDataTable();
	
		projectConfiguration.setCurrentProject(fileName);

	}
	
	public void readFiles(String fileName) {
		
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(fileName));
			String line = br.readLine();

			while (line != null) {
				if (line.contains("Project-name")) {
					String[] projectNameTable = line.split(":");
					projectConfiguration.setProjectName(projectNameTable[1]);
				} else if (line.contains("Dataset-txt")) {
					String[] datasetTxtTable = line.split(":");
					projectConfiguration.setDatasetTxt(datasetTxtTable[1]);
				} else if (line.contains("Input-csv")) {
					String[] inputCsvTable = line.split(":");
					projectConfiguration.setInputCsv(inputCsvTable[1]);
				} else if (line.contains("Assessement1-output")) {
					String[] outputAss1 = line.split(":");
					projectConfiguration.setOutputAssessment1(outputAss1[1]);
				} else if (line.contains("Assessement2-output")) {
					String[] outputAss2 = line.split(":");
					projectConfiguration.setOutputAssessment2(outputAss2[1]);
				} else if (line.contains("Transition-xml")) {
					String[] transitionXmlTable = line.split(":");
					projectConfiguration.setTransitionsFile(transitionXmlTable[1]);
				}
				line = br.readLine();
			}
			;

			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public GlobalDataKeeper getGlobalDataKeeper() {
		return globalDataKeeper;
	}

	public void setGlobalDataKeeper(GlobalDataKeeper globalDataKeeper) {
		this.globalDataKeeper = globalDataKeeper;
	}

	public ProjectConfiguration getProjectConfiguration() {
		return projectConfiguration;
	}

	public void setProjectConfiguration(ProjectConfiguration projectConfiguration) {
		this.projectConfiguration = projectConfiguration;
	}

	public TableDataArea getTableDataArea() {
		return tableDataArea;
	}

	public void setTableDataArea(TableDataArea tableDataArea) {
		this.tableDataArea = tableDataArea;
	}
	
	
	
}
