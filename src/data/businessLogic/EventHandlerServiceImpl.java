package data.businessLogic;

import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.TreePath;

import data.dataKeeper.GlobalDataKeeper;
import data.datawidgetConstruction.TableDataConstructionService;
import data.persistence.TableDataArea;
import data.persistence.TableDimensions;
import data.persistence.TableZoomDimensions;
import gui.listeners.MenuItemListener;
import gui.mainEngine.Gui;
import gui.services.EventHandlerService;
import gui.tableElements.commons.JvTable;
import gui.tableElements.tableConstructors.TableConstructionIDU;
import gui.tableElements.tableConstructors.TableConstructionPhases;
import gui.tableElements.tableRenderers.IDUTableRenderer;

public class EventHandlerServiceImpl implements EventHandlerService {

	private Gui gui;

	public EventHandlerServiceImpl(Gui gui) {
		this.gui = gui;
	}

	public void valueChangesInTree(TreeSelectionEvent ae) {
		TreePath selection = ae.getPath();
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		tableDataArea.getSelectedFromTree().add(selection.getLastPathComponent().toString());
		System.out.println(selection.getLastPathComponent().toString() + " is selected");
	}

	public void treeRelease(MouseEvent e, JTree tree) {
		if (SwingUtilities.isRightMouseButton(e)) {
			System.out.println("Right Click Tree");

			final JPopupMenu popupMenu = new JPopupMenu();
			JMenuItem showDetailsItem = new JMenuItem("Show This into the Table");
			showDetailsItem.addActionListener(new MenuItemListener(showDetailsItem, gui));

			popupMenu.add(showDetailsItem);
			popupMenu.show(tree, e.getX(), e.getY());

		}
	}

	@Override
	public void generalTableIDUClick(MouseEvent e, IDUTableRenderer renderer) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();

		if (e.getClickCount() == 1) {
			JTable target = (JTable) e.getSource();
			tableDataArea.setSelectedRowsFromMouse(target.getSelectedRows());
			tableDataArea.setSelectedColumnZoomArea(target.getSelectedColumn());
			renderer.setSelCol(tableDataArea.getSelectedColumnZoomArea());
			target.getSelectedColumns();
			gui.zoomAreaTableRepaint();
		}

	}

	@Override
	public void generalTablePhasesClick(MouseEvent e) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		if (e.getClickCount() == 1) {
			JTable target = (JTable) e.getSource();
			tableDataArea.setSelectedRowsFromMouse(target.getSelectedRows());
			tableDataArea.setSelectedColumn(target.getSelectedColumn());
			gui.lifeTimeTableRepaint();

		}

	}

	@Override
	public void zoomTableClick(MouseEvent e) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		if (e.getClickCount() == 1) {
			JTable target = (JTable) e.getSource();
			tableDataArea.setSelectedRowsFromMouse(target.getSelectedRows());
			tableDataArea.setSelectedColumnZoomArea(target.getSelectedColumn());
			gui.zoomAreaTableRepaint();
		}

	}

	@Override
	public void zoomTableForClusterClick(MouseEvent e) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		if (e.getClickCount() == 1) {
			JTable target = (JTable) e.getSource();
			tableDataArea.setSelectedRowsFromMouse(target.getSelectedRows());
			tableDataArea.setSelectedColumnZoomArea(target.getSelectedColumn());
			gui.zoomAreaTableRepaint();
		}

	}

	@Override
	public void generalTableIDURelease(MouseEvent e, JvTable table) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		if (SwingUtilities.isRightMouseButton(e)) {
			System.out.println("Right Click");

			JTable target1 = (JTable) e.getSource();
			target1.getSelectedColumns();
			tableDataArea.setSelectedRowsFromMouse(target1.getSelectedRows());
			System.out.println(target1.getSelectedColumns().length);
			System.out.println(target1.getSelectedRow());
			for (int rowsSelected = 0; rowsSelected < tableDataArea.getSelectedRowsFromMouse().length; rowsSelected++) {
				System.out.println(table.getValueAt(tableDataArea.getSelectedRowsFromMouse()[rowsSelected], 0));
			}
			final JPopupMenu popupMenu = new JPopupMenu();
			JMenuItem showDetailsItem = new JMenuItem("Clear Selection");
			showDetailsItem.addActionListener(new MenuItemListener(showDetailsItem, gui));

			popupMenu.add(showDetailsItem);
			popupMenu.show(table, e.getX(), e.getY());

		}

	}

	@Override
	public void generalTablePhasesRelease(MouseEvent e, JvTable table) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		if (e.getButton() == MouseEvent.BUTTON3) {
			System.out.println("Right Click");

			JTable target1 = (JTable) e.getSource();
			tableDataArea.setSelectedColumn(target1.getSelectedColumn());
			// selectedColumn = target1.getSelectedColumn();
			tableDataArea.setSelectedRowsFromMouse(new int[target1.getSelectedRows().length]);
			tableDataArea.setSelectedRowsFromMouse(target1.getSelectedRows());
			// selectedRowsFromMouse = new int[target1.getSelectedRows().length];
			// selectedRowsFromMouse = target1.getSelectedRows();

			final String sSelectedRow = (String) table.getValueAt(target1.getSelectedRow(), 0);

			tableDataArea.setTablesSelected(new ArrayList<String>());
			// tablesSelected = new ArrayList<String>();

			for (int rowsSelected = 0; rowsSelected < tableDataArea.getSelectedRowsFromMouse().length; rowsSelected++) {
				tableDataArea.getTablesSelected()
						.add((String) table.getValueAt(tableDataArea.getSelectedRowsFromMouse()[rowsSelected], 0));
			}

			JPopupMenu popupMenu = new JPopupMenu();
			JMenuItem showDetailsItem = new JMenuItem("Show Details for the selection");
			showDetailsItem.addActionListener(new MenuItemListener(showDetailsItem, gui, table, target1));

			popupMenu.add(showDetailsItem);
			JMenuItem clearSelectionItem = new JMenuItem("Clear Selection of LifeTimeTable");
			clearSelectionItem.addActionListener(new MenuItemListener(clearSelectionItem, gui));
			popupMenu.add(clearSelectionItem);
			popupMenu.show(table, e.getX(), e.getY());

		}

	}

	@Override
	public void zoomTableRelease(MouseEvent e, JvTable table) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		if (SwingUtilities.isRightMouseButton(e)) {
			System.out.println("Right Click");

			JTable target1 = (JTable) e.getSource();
			tableDataArea.setSelectedColumnZoomArea(target1.getSelectedColumn());
			tableDataArea.setSelectedRowsFromMouse(target1.getSelectedRows());
			System.out.println(target1.getSelectedColumn());
			System.out.println(target1.getSelectedRow());
			final ArrayList<String> tablesSelected = new ArrayList<String>();
			for (int rowsSelected = 0; rowsSelected < tableDataArea.getSelectedRowsFromMouse().length; rowsSelected++) {
				tablesSelected
						.add((String) table.getValueAt(tableDataArea.getSelectedRowsFromMouse()[rowsSelected], 0));
				System.out.println(tablesSelected.get(rowsSelected));
			}

		}

	}

	@Override
	public void zoomTableForClusterRelease(MouseEvent e, JvTable table) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		if (SwingUtilities.isRightMouseButton(e)) {
			System.out.println("Right Click");

			JTable target1 = (JTable) e.getSource();
			tableDataArea.setSelectedColumnZoomArea(target1.getSelectedColumn());
			tableDataArea.setSelectedRowsFromMouse(target1.getSelectedRows());
			System.out.println(target1.getSelectedColumn());
			System.out.println(target1.getSelectedRow());
			tableDataArea.setTablesSelected(new ArrayList<String>());

			for (int rowsSelected = 0; rowsSelected < tableDataArea.getSelectedRowsFromMouse().length; rowsSelected++) {
				tableDataArea.getTablesSelected()
						.add((String) table.getValueAt(tableDataArea.getSelectedRowsFromMouse()[rowsSelected], 0));
				System.out.println(tableDataArea.getTablesSelected().get(rowsSelected));
			}
			if (table.getColumnName(tableDataArea.getSelectedColumnZoomArea()).contains("Phase")) {

				final JPopupMenu popupMenu = new JPopupMenu();
				JMenuItem showDetailsItem = new JMenuItem("Show Details");
				showDetailsItem.addActionListener(new MenuItemListener(showDetailsItem, gui));

				popupMenu.add(showDetailsItem);
				popupMenu.show(table, e.getX(), e.getY());
			}

		}

	}

	@Override
	public void headerTableIDUClick(MouseEvent e, IDUTableRenderer renderer, JvTable table) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		tableDataArea.setWholeColZoomArea(table.columnAtPoint(e.getPoint()));
		renderer.setWholeCol(table.columnAtPoint(e.getPoint()));
		table.repaint();

	}

	@Override
	public void headerTablePhasesClick(MouseEvent e, JvTable table) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		GlobalDataKeeper globalDataKeeper = fileReader.getGlobalDataKeeper();
		tableDataArea.setWholeCol(table.columnAtPoint(e.getPoint()));

		String name = table.getColumnName(tableDataArea.getWholeCol());
		System.out.println("Column index selected " + tableDataArea.getWholeCol() + " " + name);
		table.repaint();
		if (gui.isShowingPld()) {
			TableConstructionIDU tableIDU = new TableConstructionIDU(globalDataKeeper);
			TableZoomDimensions zoomTableDimensions = new TableZoomDimensions(tableIDU.constructColumns(),
					tableIDU.constructRows(), tableIDU.getSegmentSize());
			TableDimensions tableDimensions = new TableDimensions();
			TableDataConstructionService tableService = new TableDataConstructionService(globalDataKeeper, gui,
					tableDataArea);
			gui.printGeneralTableIDU(tableService.createGeneralTableIDU(zoomTableDimensions, tableDimensions));

		}

	}

	@Override
	public void zoomHeaderTableClick(MouseEvent e, JvTable table) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		tableDataArea.setWholeColZoomArea(table.columnAtPoint(e.getPoint()));
		String name = table.getColumnName(tableDataArea.getWholeColZoomArea());
		System.out.println("Column index selected " + tableDataArea.getWholeCol() + " " + name);
		table.repaint();

	}

	@Override
	public void zoomHeaderTableForClusterClick(MouseEvent e, JvTable table) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		tableDataArea.setWholeColZoomArea(table.columnAtPoint(e.getPoint()));
		String name = table.getColumnName(tableDataArea.getWholeColZoomArea());
		System.out.println("Column index selected " + tableDataArea.getWholeCol() + " " + name);
		table.repaint();

	}

	@Override
	public void headerTableIDURelease(MouseEvent e, IDUTableRenderer renderer, JvTable table) {
		if (SwingUtilities.isRightMouseButton(e)) {
			System.out.println("Right Click");

			final JPopupMenu popupMenu = new JPopupMenu();
			JMenuItem showDetailsItem = new JMenuItem("Clear Column Selection");
			showDetailsItem.addActionListener(new MenuItemListener(showDetailsItem, gui, table, null, renderer));

			popupMenu.add(showDetailsItem);
			popupMenu.show(table, e.getX(), e.getY());

		}

	}

	@Override
	public void headerTablePhasesRelease(MouseEvent e, JvTable table) {
		if (SwingUtilities.isRightMouseButton(e)) {
			System.out.println("Right Click");

			final JPopupMenu popupMenu = new JPopupMenu();
			JMenuItem clearColumnSelectionItem = new JMenuItem("Clear Column Selection");
			clearColumnSelectionItem
					.addActionListener(new MenuItemListener(clearColumnSelectionItem, gui, table, null));

			popupMenu.add(clearColumnSelectionItem);
			JMenuItem showDetailsItem = new JMenuItem("Show Details for this Phase");
			showDetailsItem.addActionListener(new MenuItemListener(showDetailsItem, gui, table, null));

			popupMenu.add(showDetailsItem);
			popupMenu.show(table, e.getX(), e.getY());

		}

	}

	@Override
	public void zoomHeaderTableRelease(MouseEvent e, JvTable table) {
		if (SwingUtilities.isRightMouseButton(e)) {
			System.out.println("Right Click");

			final JPopupMenu popupMenu = new JPopupMenu();
			JMenuItem showDetailsItem = new JMenuItem("Clear Column Selection");
			showDetailsItem.addActionListener(new MenuItemListener(showDetailsItem, gui, table, null));

			popupMenu.add(showDetailsItem);
			popupMenu.show(table, e.getX(), e.getY());

		}

	}

	@Override
	public void zoomHeaderTableForClusterRelease(MouseEvent e, JvTable table) {
		if (SwingUtilities.isRightMouseButton(e)) {
			System.out.println("Right Click");

			final JPopupMenu popupMenu = new JPopupMenu();
			JMenuItem showDetailsItem = new JMenuItem("Clear Column Selection");
			showDetailsItem.addActionListener(new MenuItemListener(showDetailsItem, gui, table, null));

			popupMenu.add(showDetailsItem);
			popupMenu.show(table, e.getX(), e.getY());

		}

	}
	
	public void clearSelection() {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		tableDataArea.setSelectedFromTree(new ArrayList<String>());
		gui.zoomAreaTableRepaint();

	}

	public void showDetailsOfSelection(JTable jTable, JvTable table) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		InitDataServiceImpl initData = new InitDataServiceImpl();
		final String sSelectedRow = (String) table.getValueAt((jTable).getSelectedRow(), 0);
		if (sSelectedRow.contains("Cluster ")) {
			initData.showClusterSelectionToZoomArea(tableDataArea.getSelectedColumn(), sSelectedRow,gui);

		} else {
			initData.showSelectionToZoomArea(tableDataArea.getSelectedColumn(),gui);
		}
	}

	public void clearSelectionOfLifeTimeTable() {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		tableDataArea.setSelectedFromTree(new ArrayList<String>());
		gui.lifeTimeTableRepaint();
	}

	public void showDetails() {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		GlobalDataKeeper globalDataKeeper = fileReader.getGlobalDataKeeper();
		InitDataServiceImpl initData = new InitDataServiceImpl();
		TableConstructionIDU table = new TableConstructionIDU(globalDataKeeper);
		TableZoomDimensions zoomTableDimensions = new TableZoomDimensions(table.constructColumns(),
				table.constructRows(), table.getSegmentSize());
		
		tableDataArea.setFirstLevelUndoColumnsZoomArea(zoomTableDimensions.getFinalColumnsZoomArea());
		tableDataArea.setFirstLevelUndoRowsZoomArea(zoomTableDimensions.getFinalRowsZoomArea());
		initData.showSelectionToZoomArea(tableDataArea.getSelectedColumnZoomArea(),gui);
	}

	public void clearColumnSelection(JvTable table, IDUTableRenderer renderer) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		GlobalDataKeeper globalDataKeeper = fileReader.getGlobalDataKeeper();
		if (table.getName().equals("generalTableIDU")) {
			tableDataArea.setWholeColZoomArea(-1);
			renderer.setWholeCol(tableDataArea.getWholeColZoomArea());
			table.repaint();
		} else if (table.getName().equals("generalTablePhases")) {
			tableDataArea.setWholeCol(-1);
			table.repaint();
			if (gui.isShowingPld()) {
				
				TableConstructionIDU tableIDU = new TableConstructionIDU(globalDataKeeper);
				TableZoomDimensions zoomTableDimensions = new TableZoomDimensions(tableIDU.constructColumns(),
						tableIDU.constructRows(), tableIDU.getSegmentSize());
				TableDimensions tableDimensions = new TableDimensions();
				TableDataConstructionService tableService = new TableDataConstructionService(globalDataKeeper, gui,
						tableDataArea);
				gui.printGeneralTableIDU(tableService.createGeneralTableIDU(zoomTableDimensions, tableDimensions));

			}
		} else if (table.getName().equals("zoomTableForCluster")) {
			tableDataArea.setWholeColZoomArea(-1);
			table.repaint();
		} else if (table.getName().equals("zoomTable")) {
			tableDataArea.setWholeColZoomArea(-1);
			table.repaint();
		}
	}

	public void showDetailsOfPhase(JvTable table) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		GlobalDataKeeper globalDataKeeper = fileReader.getGlobalDataKeeper();
		InitDataServiceImpl initData = new InitDataServiceImpl();
		TableConstructionPhases tablePhases = new TableConstructionPhases(globalDataKeeper);
		TableDimensions tableDimensions = new TableDimensions(tablePhases.constructColumns(),
				tablePhases.constructRows(), tablePhases.getSegmentSize());
		String sSelectedRow = tableDimensions.getFinalRows()[0][0];
		System.out.println("?" + sSelectedRow);
		tableDataArea.setTablesSelected(new ArrayList<String>());
		for (int i = 0; i < tableDimensions.getFinalRows().length; i++)
			tableDataArea.getTablesSelected().add((String) table.getValueAt(i, 0));

		if (!sSelectedRow.contains("Cluster ")) {

			initData.showSelectionToZoomArea(tableDataArea.getWholeCol(),gui);
		} else {
			initData.showClusterSelectionToZoomArea(tableDataArea.getWholeCol(), "",gui);
		}
	}

	public void repaintLifeTimeTable() {
		gui.lifeTimeTableRepaint();
	}

}
