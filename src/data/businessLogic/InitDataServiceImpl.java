package data.businessLogic;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import data.dataKeeper.GlobalDataKeeper;
import data.datawidgetConstruction.TableDataConstructionService;
import data.datawidgetConstruction.TreeDataConstructionService;
import data.persistence.DetailedTableDimensions;
import data.persistence.PldConfiguration;
import data.persistence.ProjectConfiguration;
import data.persistence.TableDataArea;
import data.persistence.TableDimensions;
import data.persistence.TableZoomDimensions;
import gui.dialogs.ParametersJDialog;
import gui.mainEngine.Gui;
import gui.services.InitDataService;
import gui.tableElements.tableConstructors.PldConstruction;
import gui.tableElements.tableConstructors.TableConstructionAllSquaresIncluded;
import gui.tableElements.tableConstructors.TableConstructionClusterTablesPhasesZoomA;
import gui.tableElements.tableConstructors.TableConstructionIDU;
import gui.tableElements.tableConstructors.TableConstructionPhases;
import gui.tableElements.tableConstructors.TableConstructionWithClusters;
import gui.tableElements.tableConstructors.TableConstructionZoomArea;
import phaseAnalyzer.engine.PhaseAnalyzerMainEngine;
import tableClustering.clusterExtractor.engine.TableClusteringMainEngine;

public class InitDataServiceImpl implements InitDataService {

	private GlobalDataKeeper globalDataKeeper;
	private ProjectConfiguration projectConfiguration;
	private TableDataArea tableDataArea;
	private Gui gui;
	
	public InitDataServiceImpl() {
		
	}

	public InitDataServiceImpl(Gui gui) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		this.globalDataKeeper = fileReader.getGlobalDataKeeper();
		this.projectConfiguration = fileReader.getProjectConfiguration();
		this.tableDataArea = fileReader.getTableDataArea();
		this.gui = gui;
	}

	public void initializationOfDataTable() {

		TableConstructionIDU table = new TableConstructionIDU(globalDataKeeper);
		TableZoomDimensions zoomTableDimensions = new TableZoomDimensions(table.constructColumns(),
				table.constructRows(), table.getSegmentSize());
		TableDimensions tableDimensions = new TableDimensions();
		int numberOfPhases = globalDataKeeper.getAllPPLTransitions().size() < 56 ? 40 : 56;
		TableDataConstructionService tableService = new TableDataConstructionService(globalDataKeeper, gui,
				tableDataArea);
		gui.printGeneralTableIDU(tableService.createGeneralTableIDU(zoomTableDimensions, tableDimensions));

		PldConfiguration pld = new PldConfiguration(0.5f, 0.5f,0d,0d,0d, numberOfPhases, 14 , false, false);

		PhaseAnalyzerMainEngine mainEngine = new PhaseAnalyzerMainEngine(projectConfiguration.getInputCsv(),
				projectConfiguration.getOutputAssessment1(), projectConfiguration.getOutputAssessment2(),
				pld.getTimeWeight(), pld.getTimeWeight(), pld.getPreProcessingTime(), pld.getPreProcessingChange());

		Double b = new Double(0.3);
		Double d = new Double(0.3);
		Double c = new Double(0.3);

		mainEngine.parseInput();
		System.out.println("\n\n\n");
		mainEngine.extractPhases(numberOfPhases);

		mainEngine.connectTransitionsWithPhases(globalDataKeeper);
		globalDataKeeper.setPhaseCollectors(mainEngine.getPhaseCollectors());
		TableClusteringMainEngine mainEngine2 = new TableClusteringMainEngine(globalDataKeeper, b, d, c);
		mainEngine2.extractClusters(pld.getNumberOfClusters());
		globalDataKeeper.setClusterCollectors(mainEngine2.getClusterCollectors());
		mainEngine2.print();

		if (globalDataKeeper.getPhaseCollectors().size() != 0) {
			TableConstructionWithClusters tableP = new TableConstructionWithClusters(globalDataKeeper);
			tableDimensions = new TableDimensions(tableP.constructColumns(), tableP.constructRows(),
					tableP.getSegmentSize());

			gui.printPhasesTable(tableService.createTableGeneralTablePhases(tableDimensions));

			TreeDataConstructionService treeConstruction = new TreeDataConstructionService(globalDataKeeper);
			gui.fillTableTree(treeConstruction.getClustersTree(), "Clusters Tree");
		}

		System.out.println("Schemas:" + globalDataKeeper.getAllPPLSchemas().size());
		System.out.println("Transitions:" + globalDataKeeper.getAllPPLTransitions().size());
		System.out.println("Tables:" + globalDataKeeper.getAllPPLTables().size());

		TreeDataConstructionService treeConstruction = new TreeDataConstructionService(globalDataKeeper);
		gui.fillTableTree(treeConstruction.getGeneralTree(), "General Tree");
	}

	public void initDataOfDetailedTable() {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		this.globalDataKeeper = fileReader.getGlobalDataKeeper();
		this.projectConfiguration = fileReader.getProjectConfiguration();
		this.tableDataArea = fileReader.getTableDataArea();

		if (!(projectConfiguration.getCurrentProject() == null)) {
			TableConstructionAllSquaresIncluded table = new TableConstructionAllSquaresIncluded(globalDataKeeper);
			DetailedTableDimensions detailedTableDimensions = new DetailedTableDimensions(table.constructColumns(),
					table.constructRows(), table.getSegmentSize());

			TableDataConstructionService tableService = new TableDataConstructionService(globalDataKeeper, gui,
					tableDataArea);

			gui.printDetailedTable(tableService.createDetailedTable(detailedTableDimensions, true));
		} else {
			JOptionPane.showMessageDialog(null, "Select a Project first");
			return;
		}
	}

	public void initDataOfPLD() {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		this.globalDataKeeper = fileReader.getGlobalDataKeeper();
		this.projectConfiguration = fileReader.getProjectConfiguration();
		this.tableDataArea = fileReader.getTableDataArea();

		if (!(projectConfiguration.getCurrentProject() == null)) {

			TableConstructionIDU table = new TableConstructionIDU(globalDataKeeper);
			TableZoomDimensions zoomTableDimensions = new TableZoomDimensions(table.constructColumns(),
					table.constructRows(), table.getSegmentSize());
			TableDimensions tableDimensions = new TableDimensions();
			TableDataConstructionService tableService = new TableDataConstructionService(globalDataKeeper, gui,
					tableDataArea);
			gui.printGeneralTableIDU(tableService.createGeneralTableIDU(zoomTableDimensions, tableDimensions));

			System.out.println("Schemas: " + globalDataKeeper.getAllPPLSchemas().size());
			System.out.println("C: " + zoomTableDimensions.getFinalColumnsZoomArea().length + " R: "
					+ zoomTableDimensions.getFinalRowsZoomArea().length);

			TreeDataConstructionService treeConstruction = new TreeDataConstructionService(globalDataKeeper);
			gui.fillTableTree(treeConstruction.getGeneralTree(), "General Tree");

		} else {
			JOptionPane.showMessageDialog(null, "Select a Project first");
			return;
		}
	}

	public void initDataOfPhasesWithClustersPLD() {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		this.globalDataKeeper = fileReader.getGlobalDataKeeper();
		this.projectConfiguration = fileReader.getProjectConfiguration();
		this.tableDataArea = fileReader.getTableDataArea();
		this.tableDataArea.setWholeCol(-1);

		if (!(projectConfiguration.getProject() == null)) {

			ParametersJDialog jD = new ParametersJDialog(true);

			jD.setModal(true);

			jD.setVisible(true);

			if (jD.getConfirmation()) {
				PldConfiguration pldConfiguration = new PldConfiguration(jD.getTimeWeight(), jD.getChangeWeight(),
						jD.geBirthWeight(), jD.getDeathWeight(), jD.getChangeWeightCluster(), jD.getNumberOfPhases(),
						jD.getNumberOfClusters(), jD.getPreProcessingTime(), jD.getPreProcessingChange());

				System.out.println(pldConfiguration.getTimeWeight() + " " + pldConfiguration.getChangeWeight());

				PhaseAnalyzerMainEngine mainEngine = new PhaseAnalyzerMainEngine(projectConfiguration.getInputCsv(),
						projectConfiguration.getOutputAssessment1(), projectConfiguration.getOutputAssessment2(),
						pldConfiguration.getTimeWeight(), pldConfiguration.getChangeWeight(),
						pldConfiguration.getPreProcessingTime(), pldConfiguration.getPreProcessingChange());

				mainEngine.parseInput();
				System.out.println("\n\n\n");
				mainEngine.extractPhases(pldConfiguration.getNumberOfPhases());

				mainEngine.connectTransitionsWithPhases(globalDataKeeper);
				globalDataKeeper.setPhaseCollectors(mainEngine.getPhaseCollectors());
				TableClusteringMainEngine mainEngine2 = new TableClusteringMainEngine(globalDataKeeper,
						pldConfiguration.getBirthWeight(), pldConfiguration.getDeathWeight(),
						pldConfiguration.getChangeWeightCl());
				mainEngine2.extractClusters(pldConfiguration.getNumberOfClusters());
				globalDataKeeper.setClusterCollectors(mainEngine2.getClusterCollectors());
				mainEngine2.print();

				if (globalDataKeeper.getPhaseCollectors().size() != 0) {
					TableConstructionWithClusters table = new TableConstructionWithClusters(globalDataKeeper);
					TableDimensions tableDimensions = new TableDimensions(table.constructColumns(),
							table.constructRows(), table.getSegmentSize());
					TableDataConstructionService tableService = new TableDataConstructionService(globalDataKeeper, gui,
							tableDataArea);
					System.out.println("Schemas: " + globalDataKeeper.getAllPPLSchemas().size());
					System.out.println("C: " + tableDimensions.getFinalColumns().length + " R: " + tableDimensions.getFinalRows().length);

					gui.printPhasesTable(tableService.createTableGeneralTablePhases(tableDimensions));

					TreeDataConstructionService treeConstruction = new TreeDataConstructionService(globalDataKeeper);
					gui.fillTableTree(treeConstruction.getClustersTree(), "Clusters Tree");

				} else {
					JOptionPane.showMessageDialog(null, "Extract Phases first");
				}
			}
		} else {

			JOptionPane.showMessageDialog(null, "Please select a project first!");

		}
	}

	public void initDataOfPhasesPLD() {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		this.globalDataKeeper = fileReader.getGlobalDataKeeper();
		this.projectConfiguration = fileReader.getProjectConfiguration();
		this.tableDataArea = fileReader.getTableDataArea();
		
		if (!(projectConfiguration.getProject() == null)) {
			tableDataArea.setWholeCol(-1);
			ParametersJDialog jD = new ParametersJDialog(false);

			jD.setModal(true);

			jD.setVisible(true);

			if (jD.getConfirmation()) {
				PldConfiguration pldConfiguration = new PldConfiguration(jD.getTimeWeight(),jD.getChangeWeight(),jD.getNumberOfPhases(),jD.getPreProcessingTime(), jD.getPreProcessingChange());
			
				System.out.println(pldConfiguration.getTimeWeight() + " " + pldConfiguration.getChangeWeight());

				PhaseAnalyzerMainEngine mainEngine = new PhaseAnalyzerMainEngine(projectConfiguration.getInputCsv(),
						projectConfiguration.getOutputAssessment1(), projectConfiguration.getOutputAssessment2(), pldConfiguration.getTimeWeight(),
						pldConfiguration.getChangeWeight(), pldConfiguration.getPreProcessingTime(), pldConfiguration.getPreProcessingTime());

				mainEngine.parseInput();
				System.out.println("\n\n\n");
				mainEngine.extractPhases(pldConfiguration.getNumberOfPhases());

				mainEngine.connectTransitionsWithPhases(globalDataKeeper);
				globalDataKeeper.setPhaseCollectors(mainEngine.getPhaseCollectors());

				if (globalDataKeeper.getPhaseCollectors().size() != 0) {
					TableConstructionPhases table = new TableConstructionPhases(globalDataKeeper);
					TableDimensions tableDimensions = new TableDimensions(table.constructColumns(),
							table.constructRows(), table.getSegmentSize());
					TableDataConstructionService tableService = new TableDataConstructionService(globalDataKeeper, gui,tableDataArea);
					System.out.println("Schemas: " +globalDataKeeper.getAllPPLSchemas().size());
					System.out.println("C: " + tableDimensions.getFinalColumns().length + " R: " + tableDimensions.getFinalRows().length);

					
					gui.printPhasesTable(tableService.createTableGeneralTablePhases(tableDimensions));

					TreeDataConstructionService treeConstruction = new TreeDataConstructionService(globalDataKeeper);
					gui.fillTableTree(treeConstruction.getPhasesTree(), "Phases Tree");
				} else {
					JOptionPane.showMessageDialog(null, "Extract Phases first");
				}
			}
		} else {

			JOptionPane.showMessageDialog(null, "Please select a project first!");

		}

	}
	
	public void showSelectionToZoomArea(int selectedColumn, Gui gui) {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		GlobalDataKeeper globalDataKeeper = fileReader.getGlobalDataKeeper();

		TableConstructionZoomArea table = new TableConstructionZoomArea(globalDataKeeper, tableDataArea.getTablesSelected(),
				selectedColumn);
		TableZoomDimensions zoomTableDimensions = new TableZoomDimensions(table.constructColumns(),
				table.constructRows(), table.getSegmentSize());
		System.out.println("Schemas: " + globalDataKeeper.getAllPPLSchemas().size());
		System.out.println("C: " + zoomTableDimensions.getFinalColumnsZoomArea().length + " R: " + zoomTableDimensions.getFinalRowsZoomArea().length);


		TableDataConstructionService tableService = new TableDataConstructionService(globalDataKeeper, gui,
				tableDataArea);
		gui.printZoomTable(tableService.createZoomAreaTable(zoomTableDimensions));

	}

	public void showClusterSelectionToZoomArea(int selectedColumn, String selectedCluster, Gui gui) {

		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		TableDataArea tableDataArea = fileReader.getTableDataArea();
		GlobalDataKeeper globalDataKeeper = fileReader.getGlobalDataKeeper();
		
		ArrayList<String> tablesOfCluster = new ArrayList<String>();
		for (int i = 0; i < tableDataArea.getTablesSelected().size(); i++) {
			String[] selectedClusterSplit = tableDataArea.getTablesSelected().get(i).split(" ");
			int cluster = Integer.parseInt(selectedClusterSplit[1]);
			ArrayList<String> namesOfTables = globalDataKeeper.getClusterCollectors().get(0).getClusters().get(cluster)
					.getNamesOfTables();
			for (int j = 0; j < namesOfTables.size(); j++) {
				tablesOfCluster.add(namesOfTables.get(j));
			}
			System.out.println(tableDataArea.getTablesSelected().get(i));
		}

		PldConstruction table;
		if (selectedColumn == 0) {
			table = new TableConstructionClusterTablesPhasesZoomA(globalDataKeeper, tablesOfCluster);
		} else {
			table = new TableConstructionZoomArea(globalDataKeeper, tablesOfCluster, selectedColumn);
		}
		TableZoomDimensions zoomTableDimensions = new TableZoomDimensions(table.constructColumns(),
				table.constructRows(), table.getSegmentSize());
		System.out.println("Schemas: " + globalDataKeeper.getAllPPLSchemas().size());
		System.out.println("C: " + zoomTableDimensions.getFinalColumnsZoomArea().length + " R: " + zoomTableDimensions.getFinalRowsZoomArea().length);

		TableDataConstructionService tableService = new TableDataConstructionService(globalDataKeeper, gui,
				tableDataArea);
		gui.printZoomTableForCluster(tableService.createTableForCluster(zoomTableDimensions));


	}

}
