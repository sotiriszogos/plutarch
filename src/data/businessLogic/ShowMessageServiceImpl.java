package data.businessLogic;

import javax.swing.JOptionPane;

import gui.dialogs.ProjectInfoDialog;
import gui.services.ShowMessageService;

public class ShowMessageServiceImpl implements ShowMessageService {

	public void help() {
		String message = "To open a project, you must select a .txt file that contains the names ONLY of "
				+ "the SQL files of the dataset that you want to use." + "\n"
				+ "The .txt file must have EXACTLY the same name with the folder "
				+ "that contains the DDL Scripts of the dataset." + "\n"
				+ "Both .txt file and dataset folder must be in the same folder.";
		JOptionPane.showMessageDialog(null, message);
	}
	
	
	public void printInfo() {
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		if (!(fileReader.getProjectConfiguration().getCurrentProject() == null)) {

			System.out.println("Project Name:" + fileReader.getProjectConfiguration().getProjectName());
			System.out.println("Dataset txt:" + fileReader.getProjectConfiguration().getDatasetTxt());
			System.out.println("Input Csv:" +fileReader.getProjectConfiguration().getInputCsv());
			System.out.println("Output Assessment1:" + fileReader.getProjectConfiguration().getOutputAssessment1());
			System.out.println("Output Assessment2:" + fileReader.getProjectConfiguration().getOutputAssessment2());
			System.out.println("Transitions File:" + fileReader.getProjectConfiguration().getTransitionsFile());

			System.out.println("Schemas:" + fileReader.getGlobalDataKeeper().getAllPPLSchemas().size());
			System.out.println("Transitions:" + fileReader.getGlobalDataKeeper().getAllPPLTransitions().size());
			System.out.println("Tables:" + fileReader.getGlobalDataKeeper().getAllPPLTables().size());

			ProjectInfoDialog infoDialog = new ProjectInfoDialog(fileReader.getProjectConfiguration().getProjectName(), fileReader.getProjectConfiguration().getDatasetTxt(),
					fileReader.getProjectConfiguration().getInputCsv(), fileReader.getProjectConfiguration().getTransitionsFile(), fileReader.getGlobalDataKeeper().getAllPPLSchemas().size(),
					fileReader.getGlobalDataKeeper().getAllPPLTransitions().size(),
					fileReader.getGlobalDataKeeper().getAllPPLTables().size());

			infoDialog.setVisible(true);
		} else {
			JOptionPane.showMessageDialog(null, "Select a Project first");
			return;
		}

	}

}
