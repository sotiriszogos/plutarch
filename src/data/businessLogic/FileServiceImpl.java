package data.businessLogic;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.antlr.v4.runtime.RecognitionException;

import gui.dialogs.CreateProjectJDialog;
import gui.mainEngine.Gui;
import gui.services.FileService;

public class FileServiceImpl implements FileService{

	public FileServiceImpl() {
		
	}
	
	public void createProject(Gui gui) {
		CreateProjectJDialog createProjectDialog = new CreateProjectJDialog("", "", "", "", "", "");

		createProjectDialog.setModal(true);

		createProjectDialog.setVisible(true);

		if (createProjectDialog.getConfirmation()) {

			createProjectDialog.setVisible(false);

			File file = createProjectDialog.getFile();
			System.out.println(file.toString());
			DataFileReaderService fileReader = DataFileReaderService.getInstance();
			fileReader.getProjectConfiguration().setProject(file.getName());
			String fileName = file.toString();
			System.out.println("!!" + fileReader.getProjectConfiguration().getProject());

			try {
				fileReader.importData(fileName, gui);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Something seems wrong with this file");
				return;
			} catch (RecognitionException e) {

				JOptionPane.showMessageDialog(null, "Something seems wrong with this file");
				return;
			}

		}

	}

	public void loadProject(Gui gui) {
		String fileName = null;
		File dir = new File("filesHandler/inis");
		JFileChooser fcOpen1 = new JFileChooser();
		fcOpen1.setCurrentDirectory(dir);
		int returnVal = fcOpen1.showDialog(gui, "Open");
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		if (returnVal == JFileChooser.APPROVE_OPTION) {

			File file = fcOpen1.getSelectedFile();
			System.out.println(file.toString());
			
			fileReader.getProjectConfiguration().setProject(file.getName());
			fileName = file.toString();
			System.out.println("!!" + fileReader.getProjectConfiguration().getProject());

		} else {
			return;
		}
		try {
			fileReader.importData(fileName, gui);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Something seems wrong with this file");
			return;
		} catch (RecognitionException e) {

			JOptionPane.showMessageDialog(null, "Something seems wrong with this file");
			return;
		}

	}

	public void editProject(Gui gui) {
		String fileName = null;
		File dir = new File("filesHandler/inis");
		JFileChooser fcOpen1 = new JFileChooser();
		fcOpen1.setCurrentDirectory(dir);
		int returnVal = fcOpen1.showDialog(gui, "Open");
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		if (returnVal == JFileChooser.APPROVE_OPTION) {

			File file = fcOpen1.getSelectedFile();
			System.out.println(file.toString());
			
			fileReader.getProjectConfiguration().setProject(file.getName());
			fileName = file.toString();
			System.out.println("!!" + fileReader.getProjectConfiguration().getProject());

			fileReader.readFiles(fileName);

			System.out.println(fileReader.getProjectConfiguration().getProjectName());

			CreateProjectJDialog createProjectDialog = new CreateProjectJDialog(fileReader.getProjectConfiguration().getProjectName(),
					fileReader.getProjectConfiguration().getDatasetTxt(), fileReader.getProjectConfiguration().getInputCsv(), fileReader.getProjectConfiguration().getOutputAssessment1(), fileReader.getProjectConfiguration().getOutputAssessment2(),
					fileReader.getProjectConfiguration().getTransitionsFile());

			createProjectDialog.setModal(true);

			createProjectDialog.setVisible(true);

			if (createProjectDialog.getConfirmation()) {

				createProjectDialog.setVisible(false);

				file = createProjectDialog.getFile();
				System.out.println(file.toString());
				fileReader.getProjectConfiguration().setProject(file.getName());
				fileName = file.toString();
				System.out.println("!!" + fileReader.getProjectConfiguration().getProject());

				try {
					fileReader.importData(fileName, gui);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "Something seems wrong with this file");
					return;
				} catch (RecognitionException e) {

					JOptionPane.showMessageDialog(null, "Something seems wrong with this file");
					return;
				}

			}

		} else {
			return;
		}

	}

}
