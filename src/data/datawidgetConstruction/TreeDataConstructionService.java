package data.datawidgetConstruction;

import javax.swing.JTree;

import data.dataKeeper.GlobalDataKeeper;
import gui.treeElements.TreeConstructionGeneral;
import gui.treeElements.TreeConstructionPhases;
import gui.treeElements.TreeConstructionPhasesWithClusters;

public class TreeDataConstructionService {

	private GlobalDataKeeper globalDataKeeper;
	
	public TreeDataConstructionService(GlobalDataKeeper globalDataKeeper) {
		this.globalDataKeeper = globalDataKeeper;
	}
	
	public JTree getGeneralTree() {
		TreeConstructionGeneral tc = new TreeConstructionGeneral(globalDataKeeper);
		return tc.constructTree();
	}
	
	public JTree getPhasesTree() {
		TreeConstructionPhases tc = new TreeConstructionPhases(globalDataKeeper);
		return tc.constructTree();
	}
	
	public JTree getClustersTree() {
		TreeConstructionPhasesWithClusters tc = new TreeConstructionPhasesWithClusters(globalDataKeeper);
		return tc.constructTree();
	}
	
	
}
