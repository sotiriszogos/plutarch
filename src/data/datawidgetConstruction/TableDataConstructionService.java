package data.datawidgetConstruction;

import java.awt.Dimension;


import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.JTableHeader;

import data.dataKeeper.GlobalDataKeeper;
import data.dataSorters.PldRowSorter;
import data.persistence.DetailedTableDimensions;
import data.persistence.PldConfiguration;
import data.persistence.ProjectConfiguration;
import data.persistence.TableDataArea;
import data.persistence.TableDimensions;
import data.persistence.TableZoomDimensions;
import gui.listeners.ColumnListener;
import gui.listeners.RowListener;
import gui.listeners.TableHeaderListener;
import gui.listeners.TableListener;
import gui.mainEngine.Gui;
import gui.tableElements.commons.JvTable;
import gui.tableElements.commons.MyTableModel;
import gui.tableElements.tableRenderers.IDUHeaderTableRenderer;
import gui.tableElements.tableRenderers.IDUTableRenderer;


public class TableDataConstructionService {

	private GlobalDataKeeper globalDataKeeper;
	private Gui gui;
	private TableDataArea tableDataArea;
	
	public TableDataConstructionService(GlobalDataKeeper globalDataKeeper, Gui gui, TableDataArea tableDataArea) {
		this.globalDataKeeper = globalDataKeeper;
		this.gui = gui;
		this.tableDataArea = tableDataArea;
	}
	
	public JvTable createGeneralTableIDU(TableZoomDimensions zoomTableDimensions, TableDimensions tableDimensions) {

		PldRowSorter sorter = new PldRowSorter(zoomTableDimensions.getFinalRowsZoomArea(), globalDataKeeper);
		zoomTableDimensions.setFinalRowsZoomArea(sorter.sortRows());
		
		int numberOfColumns = zoomTableDimensions.getFinalRowsZoomArea()[0].length;
		int numberOfRows = zoomTableDimensions.getFinalRowsZoomArea().length;

		String[][] rows = new String[numberOfRows][numberOfColumns];

		for (int i = 0; i < numberOfRows; i++) {

			rows[i][0] = zoomTableDimensions.getFinalRowsZoomArea()[i][0];

		}

		MyTableModel table = new MyTableModel(zoomTableDimensions.getFinalColumnsZoomArea(), rows);

		final JvTable generalTable = new JvTable(table);
		generalTable.setName("generalTableIDU");
		generalTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		if (tableDataArea.getRowHeight() < 1) {
			tableDataArea.setRowHeight(1);
		}
		if (tableDataArea.getColumnWidth() < 1) {
			tableDataArea.setColumnWidth(1);
		}

		for (int i = 0; i < generalTable.getRowCount(); i++) {
			generalTable.setRowHeight(i, tableDataArea.getRowHeight());

		}

		generalTable.setShowGrid(false);
		generalTable.setIntercellSpacing(new Dimension(0, 0));

		for (int i = 0; i < generalTable.getColumnCount(); i++) {
			if (i == 0) {
				generalTable.getColumnModel().getColumn(0).setPreferredWidth(tableDataArea.getColumnWidth());

			} else {
				generalTable.getColumnModel().getColumn(i).setPreferredWidth(tableDataArea.getColumnWidth());

			}
		}

		int start = -1;
		int end = -1;
		if (globalDataKeeper.getPhaseCollectors() != null && tableDataArea.getWholeCol() != -1 && tableDataArea.getWholeCol() != 0) {
			start = globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(tableDataArea.getWholeCol() - 1).getStartPos();
			end = globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(tableDataArea.getWholeCol() - 1).getEndPos();
		}

		if (tableDataArea.getWholeCol() != -1) {
			for (int i = 0; i < generalTable.getColumnCount(); i++) {
				if (!(generalTable.getColumnName(i).equals("Table name"))) {
					if (Integer.parseInt(generalTable.getColumnName(i)) >= start
							&& Integer.parseInt(generalTable.getColumnName(i)) <= end) {

						generalTable.getColumnModel().getColumn(i).setHeaderRenderer(new IDUHeaderTableRenderer());

					}
				}
			}
		}

	final IDUTableRenderer renderer = new IDUTableRenderer(gui, zoomTableDimensions.getFinalRowsZoomArea(), globalDataKeeper, tableDimensions.getSegmentSize());
		// generalTable.setDefaultRenderer(Object.class, renderer);

		generalTable.setDefaultRenderer(Object.class, new TableCellRendererConstructionService(gui, 1,globalDataKeeper ,tableDimensions,zoomTableDimensions,null,tableDataArea,null)); 
		
		generalTable.addMouseListener(new TableListener(generalTable,gui,renderer));
			

		JTableHeader header = generalTable.getTableHeader();
		header.setName("headerTableIDU");
		header.addMouseListener(new TableHeaderListener(header,gui,generalTable,renderer));
		
		return generalTable;
		
	}
	
	public JvTable createTableGeneralTablePhases(TableDimensions tableDimensions)  {
		int numberOfColumns = tableDimensions.getFinalRows()[0].length;
		int numberOfRows = tableDimensions.getFinalRows().length;

		//selectedRows = new ArrayList<Integer>();

		String[][] rows = new String[numberOfRows][numberOfColumns];

		for (int i = 0; i < numberOfRows; i++) {

			rows[i][0] = tableDimensions.getFinalRows()[i][0];

		}

		MyTableModel table = new MyTableModel(tableDimensions.getFinalColumns(), rows);

		final JvTable generalTable = new JvTable(table);
		generalTable.setName("generalTablePhases");
		generalTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		generalTable.setShowGrid(false);
		generalTable.setIntercellSpacing(new Dimension(0, 0));

		for (int i = 0; i < generalTable.getColumnCount(); i++) {
			if (i == 0) {
				generalTable.getColumnModel().getColumn(0).setPreferredWidth(86);

			} else {

				generalTable.getColumnModel().getColumn(i).setPreferredWidth(1);

			}
		}

		generalTable.setDefaultRenderer(Object.class, new TableCellRendererConstructionService(gui, 2,globalDataKeeper,tableDimensions,null,null,tableDataArea,null)); 

		generalTable.addMouseListener(new TableListener(generalTable,gui));
			

		JTableHeader header = generalTable.getTableHeader();
		header.setName("headerTablePhases");
		header.addMouseListener(new TableHeaderListener(header,gui,generalTable));
		
		return generalTable;
	}
	
	
	
	public JvTable createZoomAreaTable(TableZoomDimensions tableZoomDimensions)  {
	
		int numberOfColumns = tableZoomDimensions.getFinalRowsZoomArea()[0].length;
		int numberOfRows = tableZoomDimensions.getFinalRowsZoomArea().length;

		String[][] rowsZoom = new String[numberOfRows][numberOfColumns];

		for (int i = 0; i < numberOfRows; i++) {

			rowsZoom[i][0] = tableZoomDimensions.getFinalRowsZoomArea()[i][0];

		}

		MyTableModel zoomModel = new MyTableModel(tableZoomDimensions.getFinalColumnsZoomArea(), rowsZoom);

		final JvTable zoomTable = new JvTable(zoomModel);
		zoomTable.setName("zoomTable");
		zoomTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		zoomTable.setShowGrid(false);
		zoomTable.setIntercellSpacing(new Dimension(0, 0));

		for (int i = 0; i < zoomTable.getColumnCount(); i++) {
			if (i == 0) {
				zoomTable.getColumnModel().getColumn(0).setPreferredWidth(150);

			} else {

				zoomTable.getColumnModel().getColumn(i).setPreferredWidth(20);
				zoomTable.getColumnModel().getColumn(i).setMaxWidth(20);
				zoomTable.getColumnModel().getColumn(i).setMinWidth(20);
			}
		}

		zoomTable.setDefaultRenderer(Object.class, new TableCellRendererConstructionService(gui, 3,globalDataKeeper ,null,tableZoomDimensions,null,tableDataArea,rowsZoom));

		zoomTable.addMouseListener(new TableListener(zoomTable,gui));

		// listener
		JTableHeader header = zoomTable.getTableHeader();
		header.setName("zoomHeaderTable");
		header.addMouseListener(new TableHeaderListener(header,gui, zoomTable));
		
		return zoomTable;
	}
	
	public JvTable createTableForCluster(TableZoomDimensions tableZoomDimensions) {
		int numberOfColumns = tableZoomDimensions.getFinalRowsZoomArea()[0].length;
		int numberOfRows = tableZoomDimensions.getFinalRowsZoomArea().length;
		

		final String[][] rowsZoom = new String[numberOfRows][numberOfColumns];

		for (int i = 0; i < numberOfRows; i++) {

			rowsZoom[i][0] = tableZoomDimensions.getFinalRowsZoomArea()[i][0];

		}

		MyTableModel zoomModel = new MyTableModel(tableZoomDimensions.getFinalColumnsZoomArea(), rowsZoom);

		final JvTable zoomTable = new JvTable(zoomModel);
		zoomTable.setName("zoomTableForCluster");
		zoomTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		zoomTable.setShowGrid(false);
		zoomTable.setIntercellSpacing(new Dimension(0, 0));

		for (int i = 0; i < zoomTable.getColumnCount(); i++) {
			if (i == 0) {
				zoomTable.getColumnModel().getColumn(0).setPreferredWidth(150);

			} else {

				zoomTable.getColumnModel().getColumn(i).setPreferredWidth(10);
				zoomTable.getColumnModel().getColumn(i).setMaxWidth(10);
				zoomTable.getColumnModel().getColumn(i).setMinWidth(70);
			}
		}

		zoomTable.setDefaultRenderer(Object.class, new TableCellRendererConstructionService(gui, 4,globalDataKeeper,null,tableZoomDimensions,null,tableDataArea,null));

		zoomTable.addMouseListener(new TableListener(zoomTable,gui));

		// listener
		JTableHeader header = zoomTable.getTableHeader();
		header.setName("zoomHeaderTableForCluster");
		header.addMouseListener(new TableHeaderListener(header,gui, zoomTable));
		
		return zoomTable;
	}
	
	public JvTable createDetailedTable(DetailedTableDimensions detailedTableDimensions, final boolean levelized) {
		MyTableModel	detailedModel = new MyTableModel(detailedTableDimensions.getDetailedColumns(), detailedTableDimensions.getDetailedRows());

		final JvTable tmpLifeTimeTable = new JvTable(detailedModel);

		tmpLifeTimeTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		if (levelized == true) {
			for (int i = 0; i < tmpLifeTimeTable.getColumnCount(); i++) {
				if (i == 0) {
					tmpLifeTimeTable.getColumnModel().getColumn(0).setPreferredWidth(150);
					tmpLifeTimeTable.getColumnModel().getColumn(0).setMaxWidth(150);
					tmpLifeTimeTable.getColumnModel().getColumn(0).setMinWidth(150);
				} else {
					if (tmpLifeTimeTable.getColumnName(i).contains("v")) {
						tmpLifeTimeTable.getColumnModel().getColumn(i).setPreferredWidth(100);
						tmpLifeTimeTable.getColumnModel().getColumn(i).setMaxWidth(100);
						tmpLifeTimeTable.getColumnModel().getColumn(i).setMinWidth(100);
					} else {
						tmpLifeTimeTable.getColumnModel().getColumn(i).setPreferredWidth(25);
						tmpLifeTimeTable.getColumnModel().getColumn(i).setMaxWidth(25);
						tmpLifeTimeTable.getColumnModel().getColumn(i).setMinWidth(25);
					}
				}
			}
		} else {
			for (int i = 0; i < tmpLifeTimeTable.getColumnCount(); i++) {
				if (i == 0) {
					tmpLifeTimeTable.getColumnModel().getColumn(0).setPreferredWidth(150);
					tmpLifeTimeTable.getColumnModel().getColumn(0).setMaxWidth(150);
					tmpLifeTimeTable.getColumnModel().getColumn(0).setMinWidth(150);
				} else {

					tmpLifeTimeTable.getColumnModel().getColumn(i).setPreferredWidth(20);
					tmpLifeTimeTable.getColumnModel().getColumn(i).setMaxWidth(20);
					tmpLifeTimeTable.getColumnModel().getColumn(i).setMinWidth(20);

				}
			}
		}

		tmpLifeTimeTable.setName("LifeTimeTable");
		
		tableDataArea.setCurrentLevelized(levelized);
		tmpLifeTimeTable.setDefaultRenderer(Object.class, new TableCellRendererConstructionService(gui, 5,globalDataKeeper,null,null,detailedTableDimensions,tableDataArea,null));

		tmpLifeTimeTable.setOpaque(true);

		tmpLifeTimeTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tmpLifeTimeTable.getSelectionModel().addListSelectionListener(new RowListener(gui));
		tmpLifeTimeTable.getColumnModel().getSelectionModel().addListSelectionListener(new ColumnListener());
		
		return tmpLifeTimeTable;
	}

	
}
