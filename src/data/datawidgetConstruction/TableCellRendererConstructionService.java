package data.datawidgetConstruction;

import javax.swing.*;
import java.awt.Color;
import java.awt.Component;
import javax.swing.table.TableCellRenderer;

import data.dataKeeper.GlobalDataKeeper;
import data.persistence.DetailedTableDimensions;
import data.persistence.TableDataArea;
import data.persistence.TableDimensions;
import data.persistence.TableZoomDimensions;
import gui.mainEngine.Gui;

import javax.swing.table.DefaultTableCellRenderer;

public class TableCellRendererConstructionService extends DefaultTableCellRenderer implements TableCellRenderer {

	private Gui gui;
	private int ver;
	private GlobalDataKeeper globalDataKeeper;
	private TableDimensions tableDimensions;
	private TableZoomDimensions tableZoomDimensions;
	private DetailedTableDimensions detailedTableDimensions;
	private TableDataArea tableDataArea;
	private String[][] rowsZoom;
	private static final long serialVersionUID = 1L;
	
	public TableCellRendererConstructionService(Gui gui, int ver,GlobalDataKeeper globalDataKeeper,TableDimensions tableDimensions, TableZoomDimensions tableZoomDimensions,DetailedTableDimensions detailedTableDimensions,TableDataArea area, String[][] rowsZoom) {
		this.gui = gui;
		this.ver = ver;
		this.globalDataKeeper = globalDataKeeper;
		this.tableDimensions = tableDimensions;
		this.tableZoomDimensions = tableZoomDimensions;
		this.detailedTableDimensions = detailedTableDimensions;
		this.tableDataArea = area;
		this.rowsZoom = rowsZoom;
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
				column);
		if(ver == 1) {
			return getGeneralTableIDUComponent(table, isSelected, hasFocus, row, column, c);
		} else if (ver == 2) {
			return getTableGeneralTablePhasesComponent(table, isSelected, hasFocus, row, column, c);
		} else if (ver == 3) {
			return getZoomAreaTableComponent(table, isSelected, hasFocus, row, column, c);
		} else if (ver == 4) {
			return getTableForClusterComponent(table, isSelected, hasFocus, row, column, c);
		} else if (ver == 5) {
			return getDetailedTableComponent(table, isSelected, hasFocus, row, column, c);
		} 
		return null;
	}
	
	private Component getGeneralTableIDUComponent(JTable table, boolean isSelected, boolean hasFocus, int row, int column,
			final Component c) {
		String[][] finalRowsZoomArea = tableZoomDimensions.getFinalRowsZoomArea();
		String tmpValue = finalRowsZoomArea[row][column];
		String columnName = table.getColumnName(column);
		Color fr = new Color(0, 0, 0);
		c.setForeground(fr);
		
		setOpaque(true);

		String description = DescriptionConstructionService.buildDescriptionTextForGeneralTableIDU(globalDataKeeper, table, row, column, tmpValue, isSelected, hasFocus, finalRowsZoomArea, tableDataArea);

		if(description == "") {
			Color cl = new Color(255, 69, 0, 100);
			c.setBackground(cl);
			return c;
		}else if(description != null) {
			gui.printDescription(description);
		
			Color cl = new Color(255, 69, 0, 100);
			c.setBackground(cl);
			return c;
		}
		
		return ComponentConstructionService.changeColorForGeneralTable((JComponent) c, tmpValue, columnName, tableZoomDimensions.getSegmentSizeZoomArea());
	}

	private Component getTableGeneralTablePhasesComponent(JTable table, boolean isSelected, boolean hasFocus, int row, int column,
			final Component c) {
		String [][] finalRows = tableDimensions.getFinalRows();
		String tmpValue = finalRows[row][column];
		String columnName = table.getColumnName(column);
		Color fr = new Color(0, 0, 0);
		c.setForeground(fr);

		String description = DescriptionConstructionService.buildDescriptionTextForGeneralTablePhases(globalDataKeeper, table, row, column, tmpValue, isSelected, hasFocus,finalRows,tableDataArea);

		if(description == "") {
			Color cl = new Color(255, 69, 0, 100);
			c.setBackground(cl);
			return c;
		}else if(description != null) {
			gui.printDescription(description);
		//	descriptionText.setText(description);
			Color cl = new Color(255, 69, 0, 100);
			c.setBackground(cl);
			return c;
		}
		return ComponentConstructionService.changeColorForGeneralTable((JComponent) c, tmpValue, columnName, tableDimensions.getSegmentSize());
	}
	
	private Component getZoomAreaTableComponent(JTable table, boolean isSelected, boolean hasFocus, int row, int column,
			Component c) {
		
		String [][] finalRowsZoomArea = tableZoomDimensions.getFinalRowsZoomArea();
		String tmpValue = finalRowsZoomArea[row][column];
		String columnName = table.getColumnName(column);
		Color fr = new Color(0, 0, 0);
		c.setForeground(fr);

		String description = DescriptionConstructionService.buildDescriptionTextForZoomAreaTable(globalDataKeeper,
				table, row, column, tmpValue, rowsZoom, isSelected, hasFocus, finalRowsZoomArea, tableDataArea);
		if (description != null) {
			gui.printDescription(description);
			Color cl = new Color(255, 69, 0, 100);
			c.setBackground(cl);
			return c;
		}
		return ComponentConstructionService.changeColorForZoomTable((JComponent) c, tmpValue, columnName, tableZoomDimensions.getSegmentSizeZoomArea());
	}
	
	private Component getTableForClusterComponent(JTable table, boolean isSelected, boolean hasFocus, int row, int column,
			Component c) {
		
		String [][] finalRowsZoomArea = tableZoomDimensions.getFinalRowsZoomArea();
		String tmpValue = finalRowsZoomArea[row][column];
		String columnName = table.getColumnName(column);
		Color fr = new Color(0, 0, 0);
		c.setForeground(fr);

		String description = DescriptionConstructionService.buildDescriptionTextForCluster(globalDataKeeper, table, row,
				column, tmpValue, isSelected, hasFocus,finalRowsZoomArea, tableDataArea);
		if (description != null) {
			gui.printDescription(description);
		//	descriptionText.setText(description);
			Color cl = new Color(255, 69, 0, 100);
			c.setBackground(cl);
			return c;
		}

		return ComponentConstructionService.changeColorForZoomTable((JComponent) c, tmpValue, columnName, tableZoomDimensions.getSegmentSizeZoomArea());
	}
	
	private Component getDetailedTableComponent(JTable table, boolean isSelected, boolean hasFocus, int row, int column,
			Component c) {
		String tmpValue = (String) table.getValueAt(row, column);
		String columnName = table.getColumnName(column);
		Color fr = new Color(0, 0, 0);
		c.setForeground(fr);

		if (tableDataArea.getSelectedColumn() == 0) {
			if (isSelected) {
				Color cl = new Color(255, 69, 0, 100);

				c.setBackground(cl);

				return c;
			}
		} else {
			if (isSelected && hasFocus) {

				c.setBackground(Color.YELLOW);
				return c;
			}

		}
		return ComponentConstructionService.changeColorForDetailedTable( (JComponent) c, tmpValue, columnName, tableDataArea.isCurrentLevelized(), detailedTableDimensions.getSegmentSizeDetailedTable());
	}
	
}
