package data.datawidgetConstruction;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JComponent;

public class ComponentConstructionService {
	
	public ComponentConstructionService() {
		
	}

	
	public static Component changeColorForGeneralTable(JComponent c, String tmpValue, String columnName, Integer[] segment) {
		try {
			int numericValue = Integer.parseInt(tmpValue);
			Color insersionColor = null;
			c.setToolTipText(Integer.toString(numericValue));

			if (numericValue == 0) {
				insersionColor = new Color(154, 205, 50, 200);
			} else if (numericValue > 0 && numericValue <= segment[3]) {

				insersionColor = new Color(176, 226, 255);
			} else if (numericValue > segment[3] && numericValue <= 2 * segment[3]) {
				insersionColor = new Color(92, 172, 238);
			} else if (numericValue > 2 * segment[3] && numericValue <= 3 * segment[3]) {

				insersionColor = new Color(28, 134, 238);
			} else {
				insersionColor = new Color(16, 78, 139);
			}
			c.setBackground(insersionColor);

			return c;
		} catch (Exception e) {

			if (tmpValue.equals("")) {
				c.setBackground(Color.GRAY);
				return c;
			} else {
				if (columnName.contains("v")) {
					c.setBackground(Color.lightGray);
					c.setToolTipText(columnName);
				} else {
					Color tableNameColor = new Color(205, 175, 149);
					c.setBackground(tableNameColor);
				}
				return c;
			}

		}
	}

	public static Component changeColorForZoomTable(JComponent c, String tmpValue, String columnName ,Integer[] segmentSizeZoomArea) {
		try {
			int numericValue = Integer.parseInt(tmpValue);
			Color insersionColor = null;
			c.setToolTipText(Integer.toString(numericValue));

			if (numericValue == 0) {
				insersionColor = new Color(0, 100, 0);
			} else if (numericValue > 0 && numericValue <= segmentSizeZoomArea[3]) {

				insersionColor = new Color(176, 226, 255);
			} else if (numericValue > segmentSizeZoomArea[3] && numericValue <= 2 * segmentSizeZoomArea[3]) {
				insersionColor = new Color(92, 172, 238);
			} else if (numericValue > 2 * segmentSizeZoomArea[3] && numericValue <= 3 * segmentSizeZoomArea[3]) {

				insersionColor = new Color(28, 134, 238);
			} else {
				insersionColor = new Color(16, 78, 139);
			}
			c.setBackground(insersionColor);

			return c;
		} catch (Exception e) {

			if (tmpValue.equals("")) {
				c.setBackground(Color.DARK_GRAY);
				return c;
			} else {
				if (columnName.contains("v")) {
					c.setBackground(Color.lightGray);
					c.setToolTipText(columnName);
				} else {
					Color tableNameColor = new Color(205, 175, 149);
					c.setBackground(tableNameColor);
				}
				return c;
			}

		}
	}

	public static Component changeColorForDetailedTable(JComponent c, String tmpValue, String columnName, boolean levelized, Integer[] segmentSizeDetailedTable) {
		try {
			int numericValue = Integer.parseInt(tmpValue);
			Color insersionColor = null;

			if (columnName.equals("I")) {
				if (numericValue == 0) {
					insersionColor = new Color(255, 231, 186);
				} else if (numericValue > 0 && numericValue <= segmentSizeDetailedTable[0]) {

					insersionColor = new Color(193, 255, 193);
				} else if (numericValue > segmentSizeDetailedTable[0]
						&& numericValue <= 2 * segmentSizeDetailedTable[0]) {
					insersionColor = new Color(84, 255, 159);
				} else if (numericValue > 2 * segmentSizeDetailedTable[0]
						&& numericValue <= 3 * segmentSizeDetailedTable[0]) {

					insersionColor = new Color(0, 201, 87);
				} else {
					insersionColor = new Color(0, 100, 0);
				}
				c.setBackground(insersionColor);
			}

			if (columnName.equals("U")) {
				if (numericValue == 0) {
					insersionColor = new Color(255, 231, 186);
				} else if (numericValue > 0 && numericValue <= segmentSizeDetailedTable[1]) {

					insersionColor = new Color(176, 226, 255);
				} else if (numericValue > segmentSizeDetailedTable[1]
						&& numericValue <= 2 * segmentSizeDetailedTable[1]) {
					insersionColor = new Color(92, 172, 238);
				} else if (numericValue > 2 * segmentSizeDetailedTable[1]
						&& numericValue <= 3 * segmentSizeDetailedTable[1]) {

					insersionColor = new Color(28, 134, 238);
				} else {
					insersionColor = new Color(16, 78, 139);
				}
				c.setBackground(insersionColor);
			}

			if (columnName.equals("D")) {
				if (numericValue == 0) {
					insersionColor = new Color(255, 231, 186);
				} else if (numericValue > 0 && numericValue <= segmentSizeDetailedTable[2]) {

					insersionColor = new Color(255, 106, 106);
				} else if (numericValue > segmentSizeDetailedTable[2]
						&& numericValue <= 2 * segmentSizeDetailedTable[2]) {
					insersionColor = new Color(255, 0, 0);
				} else if (numericValue > 2 * segmentSizeDetailedTable[2]
						&& numericValue <= 3 * segmentSizeDetailedTable[2]) {

					insersionColor = new Color(205, 0, 0);
				} else {
					insersionColor = new Color(139, 0, 0);
				}
				c.setBackground(insersionColor);
			}

			return c;
		} catch (Exception e) {

			if (tmpValue.equals("")) {
				c.setBackground(Color.black);
				return c;
			} else {
				if (columnName.contains("v")) {
					c.setBackground(Color.lightGray);
					if (levelized == false) {
						c.setToolTipText(columnName);
					}
				} else {
					Color tableNameColor = new Color(205, 175, 149);
					c.setBackground(tableNameColor);
				}
				return c;
			}

		}
	}
}
