package data.datawidgetConstruction;

import java.util.ArrayList;

import javax.swing.JTable;

import data.dataKeeper.GlobalDataKeeper;
import data.persistence.TableDataArea;
import gui.tableElements.commons.JvTable;

public class DescriptionConstructionService {

	public static String buildDescriptionTextForGeneralTableIDU(GlobalDataKeeper globalDataKeeper, JTable table,
			int row, int column, String tmpValue, boolean isSelected, boolean hasFocus,String[][] finalRowsZoomArea,TableDataArea tableDataArea ) {

		int wholeColZoomArea = tableDataArea.getWholeColZoomArea();
		int selectedColumnZoomArea = tableDataArea.getSelectedColumnZoomArea();
		ArrayList<String> selectedFromTree = tableDataArea.getSelectedFromTree();

		if (column == wholeColZoomArea && wholeColZoomArea != 0) {
			StringBuilder sb = new StringBuilder();
			sb.append("Transition ID:" + table.getColumnName(column) + "\n");
			sb.append("Old Version Name:" + globalDataKeeper.getAllPPLTransitions()
					.get(Integer.parseInt(table.getColumnName(column))).getOldVersionName() + "\n");
			sb.append("New Version Name:" + globalDataKeeper.getAllPPLTransitions()
					.get(Integer.parseInt(table.getColumnName(column))).getNewVersionName() + "\n");
			sb.append("Transition Changes:" + globalDataKeeper.getAllPPLTransitions()
					.get(Integer.parseInt(table.getColumnName(column))).getNumberOfChangesForOneTr() + "\n");
			sb.append("Additions:" + globalDataKeeper.getAllPPLTransitions()
					.get(Integer.parseInt(table.getColumnName(column))).getNumberOfAdditionsForOneTr() + "\n");
			sb.append("Deletions:" + globalDataKeeper.getAllPPLTransitions()
					.get(Integer.parseInt(table.getColumnName(column))).getNumberOfDeletionsForOneTr() + "\n");
			sb.append("Updates:" + globalDataKeeper.getAllPPLTransitions()
					.get(Integer.parseInt(table.getColumnName(column))).getNumberOfUpdatesForOneTr() + "\n");

			String description = sb.toString();
			return description;
		} else if (selectedColumnZoomArea == 0) {

			if (isSelected) {
				StringBuilder sb = new StringBuilder();
				sb.append("Table:" + finalRowsZoomArea[row][0] + "\n");
				sb.append("Birth Version Name:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getBirth() + "\n");
				sb.append("Birth Version ID:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getBirthVersionID() + "\n");
				sb.append("Death Version Name:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getDeath() + "\n");
				sb.append("Death Version ID:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getDeathVersionID() + "\n");
				sb.append("Total Changes:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getTotalChanges() + "\n");

				String description = sb.toString();
				return description;
			}
		} else {
			if (selectedFromTree.contains(finalRowsZoomArea[row][0])) {
				String description = "";
				return description;
			}

			if (isSelected && hasFocus) {
				String description = "";
				StringBuilder sb = new StringBuilder();
				if (!table.getColumnName(column).contains("Table name")) {

					sb.append("Table:" + finalRowsZoomArea[row][0] + "\n");
					sb.append(
							"Old Version Name:"
									+ globalDataKeeper.getAllPPLTransitions()
											.get(Integer.parseInt(table.getColumnName(column))).getOldVersionName()
									+ "\n");
					sb.append(
							"New Version Name:"
									+ globalDataKeeper.getAllPPLTransitions()
											.get(Integer.parseInt(table.getColumnName(column))).getNewVersionName()
									+ "\n");

					if (globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getTableChanges()
							.getTableAtChForOneTransition(Integer.parseInt(table.getColumnName(column))) != null) {

						sb.append("Transition Changes:" + globalDataKeeper.getAllPPLTables()
								.get(finalRowsZoomArea[row][0]).getTableChanges()
								.getTableAtChForOneTransition(Integer.parseInt(table.getColumnName(column))).size()
								+ "\n");
						sb.append("Additions:"
								+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0])
										.getNumberOfAdditionsForOneTr(Integer.parseInt(table.getColumnName(column)))
								+ "\n");
						sb.append("Deletions:"
								+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0])
										.getNumberOfDeletionsForOneTr(Integer.parseInt(table.getColumnName(column)))
								+ "\n");
						sb.append("Updates:"
								+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0])
										.getNumberOfUpdatesForOneTr(Integer.parseInt(table.getColumnName(column)))
								+ "\n");

					} else {
						sb.append("Transition Changes:0 \n");
						sb.append("Additions:0 \n");
						sb.append("Deletions:0 \n");
						sb.append("Updates:0 \n");
					}
					description = sb.toString();
				}
				return description;
			}
		}
		return null;
	}

	public static String buildDescriptionTextForGeneralTablePhases(GlobalDataKeeper globalDataKeeper, JTable table, int row,
			int column, String tmpValue, boolean isSelected, boolean hasFocus, String[][] finalRows,TableDataArea tableDataArea  ) {

		int wholeCol = tableDataArea.getWholeCol();
		int selectedColumn = tableDataArea.getSelectedColumn();
		ArrayList<String> selectedFromTree = tableDataArea.getSelectedFromTree();
		
		if (column == wholeCol && wholeCol != 0) {
			StringBuilder sb = new StringBuilder();

			sb.append(table.getColumnName(column) + "\n");
			sb.append("First Transition ID:"
					+ globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(column - 1).getStartPos() + "\n");
			sb.append("Last Transition ID:"
					+ globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(column - 1).getEndPos() + "\n");
			sb.append("Total Changes For This Phase:"
					+ globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(column - 1).getTotalUpdates()
					+ "\n");
			sb.append("Additions For This Phase:" + globalDataKeeper.getPhaseCollectors().get(0).getPhases()
					.get(column - 1).getTotalAdditionsOfPhase() + "\n");
			sb.append("Deletions For This Phase:" + globalDataKeeper.getPhaseCollectors().get(0).getPhases()
					.get(column - 1).getTotalDeletionsOfPhase() + "\n");
			sb.append("Updates For This Phase:"
					+ globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(column - 1).getTotalUpdatesOfPhase()
					+ "\n");

			String description = sb.toString();
			return description;
		} else if (selectedColumn == 0) {
			if (isSelected) {
				if (finalRows[row][0].contains("Cluster")) {

					StringBuilder sb = new StringBuilder();

					sb.append("Cluster:" + finalRows[row][0] + "\n");
					sb.append("Birth Version Name:"
							+ globalDataKeeper.getClusterCollectors().get(0).getClusters().get(row).getBirthSqlFile()
							+ "\n");
					sb.append("Birth Version ID:"
							+ globalDataKeeper.getClusterCollectors().get(0).getClusters().get(row).getBirth() + "\n");
					sb.append("Death Version Name:"
							+ globalDataKeeper.getClusterCollectors().get(0).getClusters().get(row).getDeathSqlFile()
							+ "\n");
					sb.append("Death Version ID:"
							+ globalDataKeeper.getClusterCollectors().get(0).getClusters().get(row).getDeath() + "\n");
					sb.append("Tables:" + globalDataKeeper.getClusterCollectors().get(0).getClusters().get(row)
							.getNamesOfTables().size() + "\n");
					sb.append("Total Changes:"
							+ globalDataKeeper.getClusterCollectors().get(0).getClusters().get(row).getTotalChanges()
							+ "\n");

					String description = sb.toString();
					return description;
				} else {
					StringBuilder sb = new StringBuilder();

					sb.append("Table:" + finalRows[row][0] + "\n");
					sb.append("Birth Version Name:"
							+ globalDataKeeper.getAllPPLTables().get(finalRows[row][0]).getBirth() + "\n");
					sb.append("Birth Version ID:"
							+ globalDataKeeper.getAllPPLTables().get(finalRows[row][0]).getBirthVersionID() + "\n");
					sb.append("Death Version Name:"
							+ globalDataKeeper.getAllPPLTables().get(finalRows[row][0]).getDeath() + "\n");
					sb.append("Death Version ID:"
							+ globalDataKeeper.getAllPPLTables().get(finalRows[row][0]).getDeathVersionID() + "\n");
					sb.append("Total Changes:"
							+ globalDataKeeper.getAllPPLTables().get(finalRows[row][0]).getTotalChanges() + "\n");

					String description = sb.toString();
					return description;
				}
			}
		} else {
			if (selectedFromTree.contains(finalRows[row][0])) {
				String description = "";
				return description;
			}
			if (isSelected && hasFocus) {
				if (!table.getColumnName(column).contains("Table name")) {
					if (finalRows[row][0].contains("Cluster")) {

						StringBuilder sb = new StringBuilder();

						sb.append(finalRows[row][0] + "\n");
						sb.append("Tables:" + globalDataKeeper.getClusterCollectors().get(0).getClusters().get(row)
								.getNamesOfTables().size() + "\n\n");
						sb.append(table.getColumnName(column) + "\n");
						sb.append("First Transition ID:"
								+ globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(column - 1).getStartPos()
								+ "\n");
						sb.append("Last Transition ID:"
								+ globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(column - 1).getEndPos()
								+ "\n\n");
						sb.append("Total Changes For This Phase:" + tmpValue + "\n");

						String description = sb.toString();
						return description;

					} else {
						StringBuilder sb = new StringBuilder();
						sb.append(table.getColumnName(column) + "\n");
						sb.append("First Transition ID:"
								+ globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(column - 1).getStartPos()
								+ "\n");
						sb.append("Last Transition ID:"
								+ globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(column - 1).getEndPos()
								+ "\n\n");
						sb.append("Table:" + finalRows[row][0] + "\n");
						sb.append("Birth Version Name:"
								+ globalDataKeeper.getAllPPLTables().get(finalRows[row][0]).getBirth() + "\n");
						sb.append("Birth Version ID:"
								+ globalDataKeeper.getAllPPLTables().get(finalRows[row][0]).getBirthVersionID() + "\n");
						sb.append("Death Version Name:"
								+ globalDataKeeper.getAllPPLTables().get(finalRows[row][0]).getDeath() + "\n");
						sb.append("Death Version ID:"
								+ globalDataKeeper.getAllPPLTables().get(finalRows[row][0]).getDeathVersionID() + "\n");
						sb.append("Total Changes For This Phase:" + tmpValue + "\n");

						String description = sb.toString();
						return description;
					}
				}
			}
		}
		return null;
	}

	public static String buildDescriptionTextForZoomAreaTable(GlobalDataKeeper globalDataKeeper, JTable table, int row,
			int column, String tmpValue, String[][] rowsZoom, boolean isSelected, boolean hasFocus,String[][] finalRowsZoomArea, TableDataArea tableDataArea) {

		int wholeColZoomArea = tableDataArea.getWholeColZoomArea();
		int selectedColumnZoomArea = tableDataArea.getSelectedColumnZoomArea();
		
		if (column == wholeColZoomArea) {
			StringBuilder sb = new StringBuilder();
			sb.append("Transition ID:" + table.getColumnName(column) + "\n");
			sb.append("Old Version Name:" + globalDataKeeper.getAllPPLTransitions()
					.get(Integer.parseInt(table.getColumnName(column))).getOldVersionName() + "\n");
			sb.append("New Version Name:" + globalDataKeeper.getAllPPLTransitions()
					.get(Integer.parseInt(table.getColumnName(column))).getNewVersionName() + "\n");

			sb.append("Transition Changes:" + globalDataKeeper.getAllPPLTransitions()
					.get(Integer.parseInt(table.getColumnName(column))).getNumberOfClusterChangesForOneTr(rowsZoom)
					+ "\n");

			sb.append("Additions:" + globalDataKeeper.getAllPPLTransitions()
					.get(Integer.parseInt(table.getColumnName(column))).getNumberOfClusterAdditionsForOneTr(rowsZoom)
					+ "\n");

			sb.append("Deletions:" + globalDataKeeper.getAllPPLTransitions()
					.get(Integer.parseInt(table.getColumnName(column))).getNumberOfClusterDeletionsForOneTr(rowsZoom)
					+ "\n");
			sb.append("Updates:" + globalDataKeeper.getAllPPLTransitions()
					.get(Integer.parseInt(table.getColumnName(column))).getNumberOfClusterUpdatesForOneTr(rowsZoom)
					+ "\n");

			String description = sb.toString();
			return description;
		} else if (selectedColumnZoomArea == 0) {
			if (isSelected) {
				StringBuilder sb = new StringBuilder();

				sb.append("Table:" + finalRowsZoomArea[row][0] + "\n");
				sb.append("Birth Version Name:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getBirth() + "\n");

				sb.append("Birth Version ID:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getBirthVersionID() + "\n");
				sb.append("Death Version Name:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getDeath() + "\n");

				sb.append("Death Version ID:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getDeathVersionID() + "\n");

				sb.append(
						"Total Changes:"
								+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0])
										.getTotalChangesForOnePhase(Integer.parseInt(table.getColumnName(1)),
												Integer.parseInt(table.getColumnName(table.getColumnCount() - 1)))
								+ "\n");

				String description = sb.toString();
				return description;
			}
		} else {
			if (isSelected && hasFocus) {

				if (!table.getColumnName(column).contains("Table name")) {

					StringBuilder sb = new StringBuilder();
					sb.append("Table:" + finalRowsZoomArea[row][0] + "\n");
					sb.append(
							"Old Version Name:"
									+ globalDataKeeper.getAllPPLTransitions()
											.get(Integer.parseInt(table.getColumnName(column))).getOldVersionName()
									+ "\n");
					sb.append(
							"New Version Name:"
									+ globalDataKeeper.getAllPPLTransitions()
											.get(Integer.parseInt(table.getColumnName(column))).getNewVersionName()
									+ "\n");

					if (globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getTableChanges()
							.getTableAtChForOneTransition(Integer.parseInt(table.getColumnName(column))) != null) {
						sb.append("Transition Changes:" + globalDataKeeper.getAllPPLTables()
								.get(finalRowsZoomArea[row][0]).getTableChanges()
								.getTableAtChForOneTransition(Integer.parseInt(table.getColumnName(column))).size()
								+ "\n");

						sb.append("Additions:"
								+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0])
										.getNumberOfAdditionsForOneTr(Integer.parseInt(table.getColumnName(column)))
								+ "\n");
						sb.append("Deletions:"
								+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0])
										.getNumberOfDeletionsForOneTr(Integer.parseInt(table.getColumnName(column)))
								+ "\n");
						sb.append("Updates:"
								+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0])
										.getNumberOfUpdatesForOneTr(Integer.parseInt(table.getColumnName(column)))
								+ "\n");

					} else {
						sb.append("Transition Changes:0 \n");
						sb.append("Additions:0 \n");
						sb.append("Deletions:0 \n");
						sb.append("Updates:0 \n");
					}

					String description = sb.toString();
					return description;
				}
				return null;
			}
		}
		return null;
	}

	public static String buildDescriptionTextForCluster(GlobalDataKeeper globalDataKeeper, JTable table, int row,
			int column, String tmpValue, boolean isSelected, boolean hasFocus, String[][] finalRowsZoomArea, TableDataArea tableDataArea) {
		
		int wholeColZoomArea = tableDataArea.getWholeColZoomArea();
		int selectedColumnZoomArea = tableDataArea.getSelectedColumnZoomArea();
		
		
		if (column == wholeColZoomArea && wholeColZoomArea != 0) {

			StringBuilder sb = new StringBuilder();
			sb.append(table.getColumnName(column) + "\n");
			sb.append("First Transition ID:"
					+ globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(column - 1).getStartPos() + "\n");
			sb.append("Last Transition ID:"
					+ globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(column - 1).getEndPos() + "\n");
			sb.append("Total Changes For This Phase:"
					+ globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(column - 1).getTotalUpdates()
					+ "\n");
			sb.append("Additions For This Phase:" + globalDataKeeper.getPhaseCollectors().get(0).getPhases()
					.get(column - 1).getTotalAdditionsOfPhase() + "\n");
			sb.append("Deletions For This Phase:" + globalDataKeeper.getPhaseCollectors().get(0).getPhases()
					.get(column - 1).getTotalDeletionsOfPhase() + "\n");
			sb.append("Updates For This Phase:"
					+ globalDataKeeper.getPhaseCollectors().get(0).getPhases().get(column - 1).getTotalUpdatesOfPhase()
					+ "\n");
			String description = sb.toString();
			return description;
		} else if (selectedColumnZoomArea == 0) {
			if (isSelected) {

				StringBuilder sb = new StringBuilder();
				sb.append("Table:" + finalRowsZoomArea[row][0] + "\n");
				sb.append("Birth Version Name:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getBirth() + "\n");
				sb.append("Birth Version ID:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getBirthVersionID() + "\n");
				sb.append("Death Version Name:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getDeath() + "\n");
				sb.append("Death Version ID:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getDeathVersionID() + "\n");
				sb.append("Total Changes:"
						+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getTotalChanges() + "\n");
				String description = sb.toString();
				return description;
			}
		} else {

			if (isSelected && hasFocus) {
				if (!table.getColumnName(column).contains("Table name")) {
					StringBuilder sb = new StringBuilder();
					sb.append("Transition " + table.getColumnName(column) + "\n");
					sb.append(
							"Old Version:"
									+ globalDataKeeper.getAllPPLTransitions()
											.get(Integer.parseInt(table.getColumnName(column))).getOldVersionName()
									+ "\n");
					sb.append(
							"New Version:"
									+ globalDataKeeper.getAllPPLTransitions()
											.get(Integer.parseInt(table.getColumnName(column))).getNewVersionName()
									+ "\n\n");
					sb.append("Table:" + finalRowsZoomArea[row][0] + "\n");
					sb.append("Birth Version Name:"
							+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getBirth() + "\n");
					sb.append("Birth Version ID:"
							+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getBirthVersionID()
							+ "\n");
					sb.append("Death Version Name:"
							+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getDeath() + "\n");
					sb.append("Death Version ID:"
							+ globalDataKeeper.getAllPPLTables().get(finalRowsZoomArea[row][0]).getDeathVersionID()
							+ "\n");
					sb.append("Total Changes For This Phase:" + tmpValue + "\n");
					String description = sb.toString();
					return description;
				}
			}
		}
		return null;
	}
}
