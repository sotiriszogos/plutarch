package test.gui.mainEngine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import javax.swing.JOptionPane;

import org.antlr.v4.runtime.RecognitionException;
import org.junit.Test;

import data.businessLogic.DataFileReaderService;
import data.persistence.ProjectConfiguration;
import gui.mainEngine.Gui;

public class TestLoadProject {

	Gui gui = new Gui();

	@Test
	public void testImportData() throws RecognitionException, IOException {
		Gui gui = new Gui();
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(outContent));
	    DataFileReaderService fileReader = DataFileReaderService.getInstance();
		ProjectConfiguration pc = fileReader.getProjectConfiguration();
		fileReader.importData("filesHandler/inis/Atlas.ini", gui);
		
		String expectedOutput  = 
				"Project Name:Atlas\r\n" + 
				"Dataset txt:filesHandler/datasets/Atlas.txt\r\n" + 
				"Input Csv:filesHandler/input/atlas.csv\r\n" + 
				"Output Assessment1:filesHandler/output/atlas_Assessment1.txt\r\n" + 
				"Output Assessment2:filesHandler/output/atlas_Assessment2.txt\r\n" + 
				"Transitions File:filesHandler/transitions/atlasTransitions.xml\r\n";
		assertTrue(outContent.toString().contains(expectedOutput));
		
		assertEquals(pc.getProjectName(),"Atlas");
		assertEquals(pc.getDatasetTxt(),"filesHandler/datasets/Atlas.txt");
		assertEquals(pc.getInputCsv(),"filesHandler/input/atlas.csv");
		assertEquals(pc.getOutputAssessment1(),"filesHandler/output/atlas_Assessment1.txt");
		assertEquals(pc.getOutputAssessment2(),"filesHandler/output/atlas_Assessment2.txt");
		assertEquals(pc.getTransitionsFile(),"filesHandler/transitions/atlasTransitions.xml");
	}
	
	@Test
	public void testLoadData() {
		String fileName = null;
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		ProjectConfiguration pc = fileReader.getProjectConfiguration();
		File file = new File("filesHandler/inis/Atlas.ini");
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(outContent));
		System.out.println(file.toString());
		fileReader.getProjectConfiguration().setProject(file.getName());
		fileName = file.toString();
		System.out.println("!!" + fileReader.getProjectConfiguration().getProject());

		try {
			fileReader.importData(fileName, gui);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Something seems wrong with this file");
			return;
		} catch (RecognitionException e) {

			JOptionPane.showMessageDialog(null, "Something seems wrong with this file");
			return;
		}
		String expectedOutput  = 
				"Project Name:Atlas\r\n" + 
				"Dataset txt:filesHandler/datasets/Atlas.txt\r\n" + 
				"Input Csv:filesHandler/input/atlas.csv\r\n" + 
				"Output Assessment1:filesHandler/output/atlas_Assessment1.txt\r\n" + 
				"Output Assessment2:filesHandler/output/atlas_Assessment2.txt\r\n" + 
				"Transitions File:filesHandler/transitions/atlasTransitions.xml\r\n";
		assertTrue(outContent.toString().contains(expectedOutput));
		
		assertEquals(pc.getProjectName(),"Atlas");
		assertEquals(pc.getDatasetTxt(),"filesHandler/datasets/Atlas.txt");
		assertEquals(pc.getInputCsv(),"filesHandler/input/atlas.csv");
		assertEquals(pc.getOutputAssessment1(),"filesHandler/output/atlas_Assessment1.txt");
		assertEquals(pc.getOutputAssessment2(),"filesHandler/output/atlas_Assessment2.txt");
		assertEquals(pc.getTransitionsFile(),"filesHandler/transitions/atlasTransitions.xml");
	}
}
