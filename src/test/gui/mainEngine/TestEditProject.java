package test.gui.mainEngine;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import javax.swing.JOptionPane;

import org.antlr.v4.runtime.RecognitionException;
import org.junit.Test;

import data.businessLogic.DataFileReaderService;
import data.persistence.ProjectConfiguration;
import gui.dialogs.CreateProjectJDialog;
import gui.mainEngine.Gui;

public class TestEditProject {

	Gui gui = new Gui();
	
	@Test
	public void testEditProject() {
		String fileName = null;
		DataFileReaderService fileReader = DataFileReaderService.getInstance();
		ProjectConfiguration pc = fileReader.getProjectConfiguration();

		File file = new File("filesHandler/inis/Atlas.ini");
		System.out.println(file.toString());

		fileReader.getProjectConfiguration().setProject(file.getName());
		fileName = file.toString();
		System.out.println("!!" + fileReader.getProjectConfiguration().getProject());

		fileReader.readFiles(fileName);

		System.out.println(fileReader.getProjectConfiguration().getProjectName());

		CreateProjectJDialog createProjectDialog = new CreateProjectJDialog(
				fileReader.getProjectConfiguration().getProjectName(),
				fileReader.getProjectConfiguration().getDatasetTxt(),
				fileReader.getProjectConfiguration().getInputCsv(),
				fileReader.getProjectConfiguration().getOutputAssessment1(),
				fileReader.getProjectConfiguration().getOutputAssessment2(),
				fileReader.getProjectConfiguration().getTransitionsFile());

		createProjectDialog.setModal(true);

		createProjectDialog.setVisible(true);

		if (createProjectDialog.getConfirmation()) {

			createProjectDialog.setVisible(false);

			file = createProjectDialog.getFile();
			System.out.println(file.toString());
			fileReader.getProjectConfiguration().setProject(file.getName());
			fileName = file.toString();
			System.out.println("!!" + fileReader.getProjectConfiguration().getProject());

			ByteArrayOutputStream outContent = new ByteArrayOutputStream();
			System.setOut(new PrintStream(outContent));

			try {
				fileReader.importData(fileName, gui);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Something seems wrong with this file");
				return;
			} catch (RecognitionException e) {

				JOptionPane.showMessageDialog(null, "Something seems wrong with this file");
				return;
			}

			String expectedOutput = "Project Name:" + pc.getProjectName() + "\r\n" + "Dataset txt:" + pc.getDatasetTxt()
					+ "\r\n" + "Input Csv:" + pc.getInputCsv() + "\r\n" + "Output Assessment1:"
					+ pc.getOutputAssessment1() + "\r\n" + "Output Assessment2:" + pc.getOutputAssessment2() + "\r\n"
					+ "Transitions File:" + pc.getTransitionsFile() + "\r\n";
			assertTrue(outContent.toString().contains(expectedOutput));

		}
	}
}
