package test.gui.mainEngine;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.swing.JTable;

import org.antlr.v4.runtime.RecognitionException;
import org.junit.Test;
import org.mockito.Mockito;

import data.businessLogic.DataFileReaderService;
import data.datawidgetConstruction.DescriptionConstructionService;
import data.persistence.TableDimensions;
import data.persistence.TableZoomDimensions;
import gui.mainEngine.Gui;
import gui.tableElements.tableConstructors.TableConstructionIDU;

public class TestDescriptionTexts {
	
	Gui gui = new Gui();
	DataFileReaderService fileReader = DataFileReaderService.getInstance();
	
	@Test
	public void testBuildDescriptionTextForGeneralTableIDU() throws RecognitionException, IOException {
		fileReader.importData("filesHandler/inis/Atlas.ini", gui);
		TableConstructionIDU table = new TableConstructionIDU(fileReader.getGlobalDataKeeper());
		TableZoomDimensions zoomTableDimensions = new TableZoomDimensions(table.constructColumns(),
				table.constructRows(), table.getSegmentSize());
		String [][] finalRowsZoomArea = zoomTableDimensions.getFinalRowsZoomArea();
		String tmpValue = finalRowsZoomArea[0][0];
		JTable table2 = Mockito.mock(JTable.class);
		String desc = DescriptionConstructionService.buildDescriptionTextForGeneralTableIDU(fileReader.getGlobalDataKeeper(), table2, 0, 0, tmpValue, false, false, finalRowsZoomArea, fileReader.getTableDataArea());
		assertEquals(desc, null);
	}


	@Test
	public void testBuildDescriptionTextForGeneralTablePhases() throws IOException {
		fileReader.importData("filesHandler/inis/Atlas.ini", gui);
		TableConstructionIDU table = new TableConstructionIDU(fileReader.getGlobalDataKeeper());
		TableDimensions tableDimensions = new TableDimensions(table.constructColumns(),
				table.constructRows(), table.getSegmentSize());
		String [][] finalRows = tableDimensions.getFinalRows();
		String tmpValue = finalRows[0][0];
		JTable table2 = Mockito.mock(JTable.class);
		String desc = DescriptionConstructionService.buildDescriptionTextForGeneralTablePhases(fileReader.getGlobalDataKeeper(), table2, 0, 0, tmpValue, false, false, finalRows, fileReader.getTableDataArea());
		assertEquals(desc, null);
	}

	@Test
	public void testBuildDescriptionTextForZoomAreaTable() throws IOException {
		fileReader.importData("filesHandler/inis/Atlas.ini", gui);
		TableConstructionIDU table = new TableConstructionIDU(fileReader.getGlobalDataKeeper());
		TableZoomDimensions zoomTableDimensions = new TableZoomDimensions(table.constructColumns(),
				table.constructRows(), table.getSegmentSize());
		TableDimensions tableDimensions = new TableDimensions(table.constructColumns(),
				table.constructRows(), table.getSegmentSize());
		String [][] finalRowsZoomArea = zoomTableDimensions.getFinalRowsZoomArea();
		String [][] rowsZoom = tableDimensions.getFinalRows();
		String tmpValue = finalRowsZoomArea[0][0];
		JTable table2 = Mockito.mock(JTable.class);
		String desc = DescriptionConstructionService.buildDescriptionTextForZoomAreaTable(fileReader.getGlobalDataKeeper(), table2, 0, 0, tmpValue, rowsZoom, false, false, finalRowsZoomArea, fileReader.getTableDataArea());
		assertEquals(desc, null);
	}
	
	@Test
	public void testBuildDescriptionTextForCluster() throws IOException {
		fileReader.importData("filesHandler/inis/Atlas.ini", gui);
		TableConstructionIDU table = new TableConstructionIDU(fileReader.getGlobalDataKeeper());
		TableZoomDimensions zoomTableDimensions = new TableZoomDimensions(table.constructColumns(),
				table.constructRows(), table.getSegmentSize());
		String [][] finalRowsZoomArea = zoomTableDimensions.getFinalRowsZoomArea();
		String tmpValue = finalRowsZoomArea[0][0];
		JTable table2 = Mockito.mock(JTable.class);
		String desc = DescriptionConstructionService.buildDescriptionTextForCluster(fileReader.getGlobalDataKeeper(), table2, 0, 0, tmpValue, false, false, finalRowsZoomArea, fileReader.getTableDataArea());
		assertEquals(desc, null);
	}

}
