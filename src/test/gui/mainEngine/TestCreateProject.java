package test.gui.mainEngine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import javax.swing.JOptionPane;

import org.antlr.v4.runtime.RecognitionException;
import org.junit.Test;

import data.businessLogic.DataFileReaderService;
import data.persistence.ProjectConfiguration;
import gui.dialogs.CreateProjectJDialog;
import gui.mainEngine.Gui;

public class TestCreateProject {

	Gui gui = new Gui();
	
	@Test
	public void testCreateProject() {
		CreateProjectJDialog createProjectDialog = new CreateProjectJDialog("TestProject", "filesHandler/datasets/Atlas.txt", "filesHandler/input/atlas.csv", "filesHandler/output/atlas_Assessment1.txt", "filesHandler/output/atlas_Assessment2.txt", "filesHandler/transitions/atlasTransitions.xml");

		createProjectDialog.setModal(true);

		createProjectDialog.setVisible(true);

		if (createProjectDialog.getConfirmation()) {

			createProjectDialog.setVisible(false);

			File file = createProjectDialog.getFile();
			System.out.println(file.toString());
			DataFileReaderService fileReader = DataFileReaderService.getInstance();
			ProjectConfiguration pc = fileReader.getProjectConfiguration();
			pc.setProject(file.getName());
			String fileName = file.toString();
			System.out.println("!!" + fileReader.getProjectConfiguration().getProject());
			
			ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		    System.setOut(new PrintStream(outContent));

			try {
				fileReader.importData(fileName, gui);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Something seems wrong with this file");
				return;
			} catch (RecognitionException e) {

				JOptionPane.showMessageDialog(null, "Something seems wrong with this file");
				return;
			}
			String expectedOutput  = 
					"Project Name:TestProject\r\n" + 
					"Dataset txt:filesHandler/datasets/Atlas.txt\r\n" + 
					"Input Csv:filesHandler/input/atlas.csv\r\n" + 
					"Output Assessment1:filesHandler/output/atlas_Assessment1.txt\r\n" + 
					"Output Assessment2:filesHandler/output/atlas_Assessment2.txt\r\n" + 
					"Transitions File:filesHandler/transitions/atlasTransitions.xml\r\n";
			assertTrue(outContent.toString().contains(expectedOutput));
			
			assertEquals(pc.getProjectName(),"TestProject");
			assertEquals(pc.getDatasetTxt(),"filesHandler/datasets/Atlas.txt");
			assertEquals(pc.getInputCsv(),"filesHandler/input/atlas.csv");
			assertEquals(pc.getOutputAssessment1(),"filesHandler/output/atlas_Assessment1.txt");
			assertEquals(pc.getOutputAssessment2(),"filesHandler/output/atlas_Assessment2.txt");
			assertEquals(pc.getTransitionsFile(),"filesHandler/transitions/atlasTransitions.xml");

		}
	}
}
